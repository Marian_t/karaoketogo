package myapp.karaoketogo.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import myapp.karaoketogo.Constants;
import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.MyAppContext;

import static android.R.attr.tag;

/**
 * Created by Marian on 4/22/2017.
 */

public class ModelFirebase {

    private static final String TAG = "EmailPassword";


    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    // [START declare_auth_listener]
    private FirebaseAuth.AuthStateListener mAuthListener;
    // [END declare_auth_listener]

    //----firebase storage and realtime databse-------
    private FirebaseDatabase database;//firebase databse reference

    public StorageReference mStorageRef;//firebase storage reference(read and write images)

    public ModelFirebase() {
        //------------Authentication------------
        mAuth = FirebaseAuth.getInstance();

        //initialize the FirebaseAuth instance and the AuthStateListener method so you can track whenever the user signs in or out.
        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    // User is signed in
//                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                } else {
//                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
//                }
                // [START_EXCLUDE]
                //updateUI(user);
                // [END_EXCLUDE]
            }
        };
        // [END auth_state_listener]

        //-----------End Authentication------------

        //--------------firebase database---------------
        database= FirebaseDatabase.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

    }

    /**
     * creates account in Firebase Authentication with email and password
     * @param user the user we take from the password and email,and insert the new token
     * @param listener the listener that gives us the functionality of the view in the firebase class
     */
    public void createAccount(final User user, final Model.LoginListener listener,final Model.saveUserRemote sur) {

        listener.printToLogMessage(TAG, "createAccount:" + user.getEmail());
        if (!listener.validateFormInRegister()) {
            return;
        }

        listener.showProgressBar();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(listener.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        listener.printToLogMessage(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            listener.makeToastAuthFailed();
                            listener.hideProgressBar();
                        }
                        else
                        {
                            listener.updateRegisterActivityIfSuccess();

                            //now adding user to loacl and remote database

                            //enter the token that we got from firebase into userID
                            user.setUserId(mAuth.getCurrentUser().getUid());

                            sur.saveUserToRemote(user);
                        }


                    }
                });
        // [END create_user_with_email]


    }

    public void addUser(User user,final Model.LoginListener viewlistener)
    {
        viewlistener.printToLogMessage("TAG","Image was saved to Firebase storage");

        //saving user deatails to storage
        HashMap<String, Object> result = new HashMap<>();
        result.put("firstName",user.getFirstName());
        result.put("lastName",user.getLastName());
        result.put("points",user.getPoints());


        DatabaseReference myRef = database.getReference("users").child(user.getUserId());
        myRef.setValue(result);

        viewlistener.hideProgressBar();



    }

    /**
     * send email verification to verify user email
     * @param listener listener which generates functions from the Activity
     */
    public void sendEmailVerification(final Model.LoginListener listener) {

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser fbUser = mAuth.getCurrentUser();
        //first checking that we make account
        if (fbUser != null) {
            fbUser.sendEmailVerification()
                    .addOnCompleteListener(listener.getActivity(), new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // [START_EXCLUDE]
                            // Re-enable button

                            if (task.isSuccessful()) {
                                listener.makeToastVerifyEmail("Verification email sent to " + fbUser.getEmail());
                            } else {
                                listener.printToLogException(TAG, "sendEmailVerification", task.getException());
                                listener.makeToastVerifyEmail("Failed to send verification email.");
                            }
                            // [END_EXCLUDE]
                        }
                    });
        }
        else
        {
            listener.makeToastVerifyEmail("Can't verify Email before making account");
        }
        // [END send_email_verification]
    }

    public void signInAfterRegister(String email, String password,final Model.LoginListener listener)
    {

        //you have to sign out  and then sign in,in order to get the new email verified status
        mAuth.signOut();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(listener.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        listener.printToLogMessage(TAG,"signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            listener.printToLogWarning(TAG, "signInWithEmail:failed", task.getException());
                            listener.makeToastAuthFailed();

                        }
                        FirebaseUser fbUser=mAuth.getCurrentUser();
                        if(fbUser!=null)
                        {
                            if(mAuth.getCurrentUser().isEmailVerified())
                            {
                                listener.goToKaraokeActivity();//moved to changeStatus in firebase
                            }
                            else
                            {
                                listener.makeToastVerifyEmail("Please verify email first before signing in");
                                //signOut();

                            }

                        }
                        listener.hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]

    }
    public void signIn(String email, String password, final Model.LoginListener listener) {
        listener.printToLogMessage(TAG, "signIn:" + email);
        if (!listener.validateFormInSignIn()) {
            return;
        }

        listener.showProgressBar();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(listener.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        listener.printToLogMessage(TAG,"signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            listener.printToLogWarning(TAG, "signInWithEmail:failed", task.getException());
                            listener.makeToastAuthFailed();

                        }
                        else
                        {
                            listener.goToKaraokeActivity();
                        }
                        listener.hideProgressBar();

                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    public void checkIfUserAuthonticated(Model.LoginListener loginListener) {
        if(mAuth.getCurrentUser()!=null)
        {
            loginListener.goToKaraokeActivity();
        }
    }

    public void signOut(Model.KaraokeListener karaokeListener) {

        karaokeListener.showProgressBar();
        mAuth.signOut();
        karaokeListener.hideProgressBar();
        karaokeListener.goToMainActivity();
    }

    public void uploadSongs() {
        ArrayList<Song> songsToUpload=new ArrayList<>();

        //1. Abba - Dancing Queen
        songsToUpload.add(new Song(null,"Dancing Queen","Abba",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FABBA%20-%20Dancing%20Queen.mp3?alt=media&token=00847a61-435c-423e-b510-31d51c01f6a7",
                null,"xFrGuyw1V8s",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAbba%20-%20Dancing%20queen.txt?alt=media&token=6c082640-4299-499c-9c20-96ec02655e2d",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAbba-%20Dancing%20Queen.jpg?alt=media&token=3b14bdd8-7f4b-4543-99c7-d64f48dcdca9"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAbba%20-%20Dancing%20Queen.mp3?alt=media&token=4243d4df-c66b-44ee-a311-31d7ffda4e42",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FABBA%20-%20Dancing%20Queen.mp4?alt=media&token=401d3bd0-bedd-449e-bc12-c570a593d07d",
                null
        ));

        //2. ADELE - Skyfall
        songsToUpload.add(new Song(null,"Skyfall","Adele",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAdele%20-%20Skyfall.mp3?alt=media&token=b92c7387-701b-4d09-a2f4-4219561cf674"
                ,null,"7HKoqNJtMTQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAdele%20-%20Skyfall.txt?alt=media&token=b2941aad-d60a-4a56-bbe7-18cdba84a360",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAdele-%20Skyfall.jpg?alt=media&token=f491fb9a-0638-4f1e-8f9a-126601389c88"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FADELE%20-%20Skyfall.mp3?alt=media&token=823a05c6-39fe-4461-bfd5-67a83e722ca4",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAdele%20-%20Skyfall.mp4?alt=media&token=5a3f3169-09e1-40c9-b139-f4b2e1f4487e"
                ,null
        ));

        //3. Adele - Someone Like You
        songsToUpload.add(new Song(null,"Someone Like You","Adele",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAdele%20-%20Someone%20Like%20You.mp3?alt=media&token=05b0a915-7ce6-4f16-b0be-3e7500a49204"
                ,null,"hLQl3WQQoQ0",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAdele%20-%20Someone%20Like%20You.txt?alt=media&token=90935b18-f574-44d3-b50e-7eb6e05c7876",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAdele-%20Someone%20Like%20You.jpg?alt=media&token=f0b5c16c-2773-4c83-85dd-cbdfaffb8319"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAdele%20-%20Someone%20Like%20You.mp3?alt=media&token=5bf94f66-1963-4055-94a8-9c4eb0230193",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAdele%20-%20Someone%20Like%20You.mp4?alt=media&token=cf90138f-cde6-4622-a0f9-a6052df9edc1",
                null
        ));

        //4.Adele--Set-Fire-To-The-Rain
        songsToUpload.add(new Song(null,"Set Fire To The Rain","Adele",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAdele%20-%20Set%20Fire%20To%20The%20Rain.mp3?alt=media&token=578df72c-6fa8-4b94-8852-736c3066bde2",
                null,"FlsBObg-1BQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAdele%20-%20Set%20Fire%20to%20the%20Rain.txt?alt=media&token=0ddf5d34-21f1-4d9c-afd3-9020a4303e20",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAdele-%20Set%20fire%20to%20the%20rain.jpg?alt=media&token=a7256ded-2c59-4155-bdd6-769aa176f228"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAdele--Set-Fire-To-The-Rain.mp3?alt=media&token=06eca894-7aa0-4e9b-9d11-dadf362c52b5",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAdele%20-%20Set%20Fire%20To%20The%20Rain.mp4?alt=media&token=de780d63-2f66-4052-988d-e41b0bd2652e",
                null
        ));

        //5.Alicia Keys - If I Aint Got You
        songsToUpload.add(new Song(null,"If I Aint Got You","Alicia Keys",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAlicia%20Keys%20-%20If%20I%20Aint%20Got%20You.mp3?alt=media&token=bb807162-d76b-4752-b3af-7030638cc94a",
                null,"Ju8Hr50Ckwk",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAlicia%20Keys%20-%20If%20I%20Aint%20Got%20You.txt?alt=media&token=2d4692fe-5d52-484b-9752-e708a066dcab",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAlicia%20Keys%20-%20If%20I%20Aint%20Got%20You.png?alt=media&token=d84807d3-b903-4f7e-831c-a092faf338b9"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAlicia%20Keys%20-%20If%20I%20Aint%20Got%20You.mp3?alt=media&token=df0cf097-5bf2-4e6a-ab5b-fdf9a5ad5eb2",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAdele%20-%20Set%20Fire%20To%20The%20Rain.mp4?alt=media&token=de780d63-2f66-4052-988d-e41b0bd2652e",
                null
        ));

        //6. Amy Winehouse - Tears Dry On Their Own
        songsToUpload.add(new Song(null,"Tears Dry On Their Own","Amy Winehouse",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAmy%20Winehouse%20-%20Tears%20Dry%20on%20There%20Own.mp3?alt=media&token=13327388-e7d4-413d-b013-8fcfaf2b482b",
                null,"ojdbDYahiCQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAmy%20Winehouse%20-%20Tears%20Dry%20On%20Their%20Own.txt?alt=media&token=c1bcc979-bedd-41e5-86d4-df92909136c3",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAmy%20Winehouse%20-%20Tears%20Dry%20On%20Their%20Own.jpg?alt=media&token=4acc8871-d844-474f-9a7f-66b874a2b5c6"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAmy%20Winehouse%20-%20Tears%20Dry%20On%20Their%20Own.mp3?alt=media&token=493fe961-f6b4-493a-90f8-1c30440d19df",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAmy%20Winehouse%20-%20Tears%20Dry%20on%20There%20Own.mp4?alt=media&token=9fe96175-6501-426f-86cd-b2f9c58f99a3"
                ,null
        ));

        //7. Aqua - Doctor Jones
        songsToUpload.add(new Song(null,"Doctor Jones","Aqua",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAqua%20-%20Doctor%20Jones.mp3?alt=media&token=7f5c2e73-7142-4feb-a0fb-3c618b939ae2",
                null,"adBOcofSxAs",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAqua%20-%20Doctor%20Jones.txt?alt=media&token=8a455eb0-764c-4a6f-b02c-725d52d93032",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAqua%20-%20Doctor%20jones.jpg?alt=media&token=be35ee4c-b526-4487-b4bf-ba50ee1915a9"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAqua%20-%20Dr%20Jones.flv.mp3?alt=media&token=d1a51f4c-7da3-4724-ab43-d8f24a19ae00",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAmy%20Winehouse%20-%20Tears%20Dry%20on%20There%20Own.mp4?alt=media&token=9fe96175-6501-426f-86cd-b2f9c58f99a3",
                null

        ));

        //8. Avicii - The Nights
        songsToUpload.add(new Song(null,"The Nights","Avicii",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FAvicii%20-%20the%20night.mp3?alt=media&token=cfe3e586-18e8-4070-981d-5078153754f9",
                null,"XmIgg9De9hY",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FAvicii%20-%20The%20Nights.txt?alt=media&token=571dd617-e58a-453a-9aaa-d5ebc1071414",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FAvicii-%20The%20nights.jpg?alt=media&token=5e55b1b7-8133-4557-8884-4d4bd2bdbd9e"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FAvicii%20-%20The%20Nights.mp3?alt=media&token=9f6cf001-1fe3-4889-bd9a-93a9d661d6e5",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FAvicii%20-%20the%20night.mp4?alt=media&token=f17d715f-35bc-41f5-b2ef-c746d20787a7",
                null
        ));

        //9. Beyonce - Halo
        songsToUpload.add(new Song(null,"Halo","Beyonce",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FBeyonce%20-%20Halo.mp3?alt=media&token=6347a776-1f8b-4424-856f-38f071b04041",
                null,"VyR7yoDBQSg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FBeyonce%20-%20halo.txt?alt=media&token=461219f9-d72a-4d95-8080-5c47fa3b6649",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FBeyonce%20-%20halo.jpg?alt=media&token=e029eaf6-9d09-44d3-bd35-cbea1a645b67"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FBeyonc%C3%A9%20-%20Halo.mp3?alt=media&token=9c1ec56e-fc3c-48b8-86ba-22125d8895b3",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FBeyonce%20-%20Halo.mp4?alt=media&token=7bd01704-494e-4b77-a30d-0dca30861d10",
                null
        ));

        //10. Bon Jovi - It's my life
        songsToUpload.add(new Song(null,"It's my life","Bon Jovi",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FBon%20Jovi%20-%20It's%20my%20life.mp3?alt=media&token=8af74c1a-ced8-4105-a375-c806b861f192",
                null,"OQIHTe7cYCg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FBon%20Jovi%20-%20it's%20my%20life.txt?alt=media&token=c598daf2-f198-41b0-8dfc-b31f51eb5be0",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FBon%20Jovi%20-%20it's%20my%20life.jpg?alt=media&token=1dac19f6-a617-4402-92ce-06ca72eb2a11"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FBon%20Jovi%20-%20It's%20my%20life.mp3?alt=media&token=ef40ac5c-016d-45a8-ab03-8bc7d05f80b8",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FBon%20Jovi%20-%20It's%20my%20life.mp4?alt=media&token=5942be92-fe0a-469c-9fde-aab3ecfc94e9",
                null
        ));


        //11.Britney Spears - Everytime
        songsToUpload.add(new Song(null,"Everytime","Britney Spears",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FBritney%20Spears-%20Everytime.mp3?alt=media&token=3f1895a9-6d5e-4586-9e72-92374d42fbd1",
                null,"tSdPwvo5ArI",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FBritney%20Spears%20-%20Everytime.txt?alt=media&token=6a23769f-5bd9-459c-ae76-627f06c4bb2b",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FBritney%20Spears-%20Every%20time.jpg?alt=media&token=d9e9d93e-5c0a-45d1-89c5-4f6b8e9a18bf"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FBritney%20Spears%20-%20Everytime.mp3?alt=media&token=66188d02-ab1c-42ef-a07a-8633150d672f",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FBritney%20Spears%20-%20Everytime.mp4?alt=media&token=1167e1cc-ebe9-4ffc-ad74-72207a10719e",
                null
        ));

        //12. Bruno Mars - 24K Magic
        songsToUpload.add(new Song(null,"24K Magic","Bruno Mars",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FBruno%20Mars%20-%2024K%20Magic.mp3?alt=media&token=92b82a0c-0006-4177-94dd-4ffcdcf52bef",
                null,"UqyT8IEBkvY",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FBruno%20Mars%20-%2024K%20Magic.txt?alt=media&token=f814c203-47c7-4026-b57c-7b96383f8b47",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FBruno%20mars-%2024%20magic.jpg?alt=media&token=ea896fbd-3e76-4bad-aef2-b90d0d212121"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FBruno%20Mars%20-%2024K%20Magic.mp3?alt=media&token=9ab57408-abae-4e69-bc70-86f24ca0c9c1",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FBruno%20Mars%20-%2024K%20Magic.mp4?alt=media&token=4cf0f5f2-d53a-4874-b81a-165b01a63236",
                null
        ));

        //13. Capital Cities - Safe And Sound
        songsToUpload.add(new Song(null,"Safe And Sound","Capital Cities",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FCapital%20Cities%20-%20Safe%20And%20Sound.mp3?alt=media&token=0cf60aa0-35da-4980-ae40-277eae9ef0c0",
                null,"P-u_1gNcmek",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FCapital%20Cities%20-%20Safe%20And%20Sound.txt?alt=media&token=fb37c4f1-d9c8-44a6-9e23-b2dcaf774144",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FCapital%20cities-%20Safe%20And%20Sound.jpg?alt=media&token=12435ed7-3161-4bd2-b31c-1c6545dd7f93"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FCapital%20Cities%20-%20Safe%20and%20Sound.mp3?alt=media&token=d73784a1-577c-498d-91ef-5b6c07ffc072",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FCapital%20Cities%20-%20Safe%20And%20Sound.mp4?alt=media&token=d13d1617-22b8-4fcc-973e-7231aff58bac",
                null
        ));

        //14. Carly Rae Jepsen - Call Me Maybe
        songsToUpload.add(new Song(null,"Call Me Maybe","Carly Rae Jepsen",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FCarly%20Rae%20Jepsen%20-%20Call%20Me%20Maybe.mp3?alt=media&token=cfea1b38-7845-4e8c-ad94-9d257ae361cb",
                null,"fWNaR-rxAic",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FCarly%20Rae%20Jepsen%20-%20Call%20Me%20Maybe.txt?alt=media&token=50545264-d5c0-400f-9788-eb92a1044864",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FCarly%20Rae%20Jepsen-%20Call%20me%20maybe.jpg?alt=media&token=adbdf7e8-57e2-4554-854f-c01f1b620a68"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FCarly%20Rae%20Jepsen%20-%20Call%20Me%20Maybe.mp3?alt=media&token=597189a2-f554-4ba7-8a11-98bc419970d0",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FCarly%20Rae%20Jepsen%20-%20Call%20Me%20Maybe.mp4?alt=media&token=e2c728ab-1879-4cba-a871-60580254f9e3",
                null
        ));

        //15. Celine Dion - My Heart Will Go On
        songsToUpload.add(new Song(null,"My Heart Will Go On","Celine Dion",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FCeline%20Dion%20-%20My%20Heart%20Will%20Go%20On.mp3?alt=media&token=4312fefc-8f91-4bb6-8329-c3836a3ea6c5",
                null,"FHG2oizTlpY",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FCeline%20Dion%20-%20My%20Heart%20Will%20Go%20On.txt?alt=media&token=1ca13ee2-52d5-4d8e-b99c-2e7ec507f79d",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FCeline_dion-my_heart.jpg?alt=media&token=1b2d4bb7-8e68-434d-994d-5e015a6cd0f8"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FCeline%20Dion%20-%20My%20Heart%20Will%20Go%20On.mp3?alt=media&token=15b54cf7-a15a-433a-9dd6-cd856fe7e1f6",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FCeline%20Dion%20-%20My%20Heart%20Will%20Go%20On.mp4?alt=media&token=b466ddd6-1469-4296-aed5-d76bb3374d6f",
                null
        ));

        //16. Christina Perri - Jar of Hearts
        songsToUpload.add(new Song(null,"Jar of Hearts","Christina Perri",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FChristina%20Perri%20-%20Jar%20of%20Hearts.mp3?alt=media&token=5b108e5b-4d18-437c-a283-90dbfa4aa672",
                null,"8v_4O44sfjM",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FChristina%20Perri%20-%20Jar%20of%20Hearts.txt?alt=media&token=1fd853c5-00a9-4a63-a4e4-227a0a62d088",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FChristina%20Perri-%20Jar%20of%20hearts.jpg?alt=media&token=9770e08e-99c2-4b17-aae3-801edad9b2f0"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FChristina%20Perri%20-%20Jar%20of%20Hearts.mp3?alt=media&token=10ffd538-beb7-43c3-b2e6-5ec4639d7ae1",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FChristina%20Perri%20-%20Jar%20of%20Hearts.mp4?alt=media&token=f71ee85e-5d31-4bf8-816e-a3c31c4a8f8c",
                null
        ));

        //17. Clean Bandit feat. Jess Glynne - Rather Be
        songsToUpload.add(new Song(null,"Rather Be","Clean Bandit feat. Jess Glynne",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FClean%20Bandit%20-%20Rather%20Be.mp3?alt=media&token=885b0d8e-1a05-4ef7-a2be-d7a8f571b692",
                null,"tQDSJYXRU_M",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FClean%20Bandit%20%26%20Jess%20Glynne%20-%20Rather%20Be.txt?alt=media&token=ae77081b-2d25-4e9a-b511-01073d72ee3f",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FRather%20Be.jpg?alt=media&token=bb697cd0-f92b-46d9-8018-cdce6f79b322"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FClean%20Bandit%20-%20Rather%20Be.mp3?alt=media&token=78c1a08d-c400-44ef-8f8e-301e90c47a5f",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FClean%20Bandit%20-%20Rather%20Be.mp4?alt=media&token=224474d2-c8b7-44f7-ba03-9cea6cf54882",
                null
        ));

        //18. Clean Bandit ft. Sean Paul & Anne-Marie- Rockabye
        songsToUpload.add(new Song(null,"Rockabye","lean Bandit ft. Sean Paul & Anne-Marie",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FClean%20Bandit%20ft.%20Sean%20Paul%20%26%20Anne-Marie%20-%20Rockabye.mp3?alt=media&token=23117449-6961-4e50-8a21-8f402092e084",
                null,"papuvlVeZg8",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FClean%20Bandit%20-%20Rockabye%20ft.%20Sean%20Paul%20%26%20Anne-Mariet.txt?alt=media&token=895c6941-ea03-4a24-828c-92e25fc72eae",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Fclean-bandit-rockabye.jpg?alt=media&token=38f332fc-f267-4de5-8abc-c1914c3fbe96"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FClean%20Bandit%20-%20Rockabye%20ft.%20Sean%20Paul%20%26%20Anne-Marie.mp3?alt=media&token=a13011c4-a078-43d0-b109-85af46f313f0",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FClean%20Bandit%20ft.%20Sean%20Paul%20%26%20Anne-Marie%20-%20Rockabye.mp4?alt=media&token=ca03ab79-67e9-4ce3-a199-40cb0f945df0",
                null
        ));

        //19. Coldplay - A Sky Full Of Stars
        songsToUpload.add(new Song(null,"A Sky Full Of Stars","Coldplay",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FColdplay%20-%20A%20Sky%20Full%20of%20Stars.mp3?alt=media&token=10770937-f427-4561-ab7e-0a29b1967efc",
                null,"LR73DrKX_bs",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FColdplay%20-%20A%20Sky%20Full%20Of%20Stars.txt?alt=media&token=7b76946d-c74e-4ad5-882c-46715fee9342",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FColdplay-_A%20Sky%20Full%20of%20Stars.jpg?alt=media&token=af0b1f76-75b2-49b6-a99a-a931297b10928"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FColdplay%20-%20A%20Sky%20Full%20of%20Stars.mp3?alt=media&token=a9e3ca7d-1834-4937-ab25-c140086b744b",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FColdplay%20-%20A%20Sky%20Full%20of%20Stars.mp4?alt=media&token=a86a6d1c-7348-4315-acb1-2e9eb0f9d372",
                null
        ));

        //20. Coldplay - Fix You
        songsToUpload.add(new Song(null,"Fix You","Coldplay",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FColdplay%20-%20Fix%20You.mp3?alt=media&token=4ce8baaa-ea10-4ed2-aa8a-b8c32c0da2dc",
                null,"k4V3Mo61fJM",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FColdplay%20-%20Fix%20you.txt?alt=media&token=a902510c-97c8-45c0-99d2-474fe6d7614b",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Ffix_you.jpg?alt=media&token=416b07a6-7d3c-47db-816f-a65fdfdfe6a3"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FColdplay%20-%20Fix%20You.mp3?alt=media&token=efb7da8d-243a-48c0-9c63-76e78c47b0f9",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FColdplay%20-%20Fix%20You.mp4?alt=media&token=83dc33bb-4d69-4b52-af4d-61684bf817d7",
                null
        ));

        //21. David Guetta - Titanium ft. Sia
        songsToUpload.add(new Song(null,"Titanium","David Guetta",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FDavid%20Guetta%20Ft.%20Sia%20-%20Titanium.mp3?alt=media&token=c64240ed-30c8-4f77-84a4-965b949b456f",
                null,"JRfuAukYTKg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FDavid%20Guetta%20-%20Titanium%20ft.%20Sia.txt?alt=media&token=66f0b7c2-0049-4168-9bd8-b2c6924025ab",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FDavid%20guetta-%20Titanium.jpg?alt=media&token=508769ee-528a-48d4-b62e-8aba00af31b0"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FDavid%20Guetta%20-%20Titanium%20ft.%20Sia.mp3?alt=media&token=6a365944-8ef9-4bc0-9541-ec289de623c6",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FDavid%20Guetta%20Ft.%20Sia%20-%20Titanium.mp4?alt=media&token=5fce6546-945d-43d7-8b61-1538f893d78b",
                null
        ));

        //22. DJ Snake ft. Justin Bieber- Let Me Love You
        songsToUpload.add(new Song(null,"Let Me Love You","DJ Snake ft. Justin Bieber",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FDJ%20Snake%20ft.%20Justin%20Bieber%20-%20Let%20Me%20Love%20You.mp3?alt=media&token=e2599322-a9ee-43ee-b15e-90ee2f590cc0",
                null,"euCqAq6BRa4",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FDJ%20Snake%20ft.%20Justin%20Bieber%20-%20Let%20Me%20Love%20You.txt?alt=media&token=999bef70-4c1b-4c5a-80cd-a96b558a52d2",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FLet%20me%20love%20you.jpg?alt=media&token=647f093e-6d08-4bf4-9cff-94c6f20d8697"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FDJ%20Snake%20-%20Let%20Me%20Love%20You%20ft.%20Justin%20Bieber.mp3?alt=media&token=67cd3621-5f7a-4e1c-8404-9405ac443950",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FDJ%20Snake%20ft.%20Justin%20Bieber%20-%20Let%20Me%20Love%20You.mp4?alt=media&token=cc70f1e9-8405-4070-a31d-655fa00e313a",
                null
        ));

        //23. Ed Sheeran - I See Fire
        songsToUpload.add(new Song(null,"I See Fire","Ed Sheeran",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FEd%20Sheeran%20-%20I%20See%20Fire.mp3?alt=media&token=32de661c-bf3b-44c6-b53c-30dc2e7a2630",
                null,"2fngvQS_PmQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FEd%20Sheeran%20-%20I%20See%20Fire.txt?alt=media&token=27521a66-612c-43af-93b4-23576f570335",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FEd%20Sheeran%20-%20I%20See%20Fire.jpg?alt=media&token=4935990d-c8a9-4778-8621-bfeb4bd364b8"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FEd%20Sheeran%20-%20I%20See%20Fire.mp3?alt=media&token=c1c325f2-7e77-46c1-8dcc-077816004486",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FEd%20Sheeran%20-%20I%20See%20Fire.mp4?alt=media&token=b375f099-b2c7-45a1-8349-60ddc0b2605f",
                null
        ));

        //24. Ed Sheeran - Shape Of You
        songsToUpload.add(new Song(null,"Shape Of You","Ed Sheeran",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FEd%20Sheeran%20-%20Shape%20of%20You.mp3?alt=media&token=7d993759-bf05-4b39-b267-272aa2ca5adc",
                null,"_dK2tDK9grQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FEd%20Sherran%20-%20Shape%20Of%20You.txt?alt=media&token=e3ad7177-8e90-4062-b891-08d4f04032a4",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Fshape_of_ed.jpg?alt=media&token=cf350a1d-5017-42f7-80f9-be7b2235afbc"
                ,null,null,
                "hhttps://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FEd%20Sheeran%20-%20Shape%20Of%20You.mp3?alt=media&token=7cde61ab-e53d-49da-9bab-f21e99a18656",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FEd%20Sheeran%20-%20Shape%20of%20You.mp4?alt=media&token=64ac1bdf-cf3b-42f5-81ba-ade10d83994b",
                null
        ));

        //25. Ed Sheeran - Thinking Out Loud
        songsToUpload.add(new Song(null,"Thinking Out Loud","Ed Sheeran",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FEd%20Sheeran%20-%20Thinking%20Out%20Loud.mp3?alt=media&token=bf6ff39f-a449-4b68-aa30-6c850b508829",
                null,"WpyfrixXBqU",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FEd%20Sheeran%20-%20Thinking%20out%20Loud.txt?alt=media&token=c531ea3a-9bc4-4740-ada4-63c5c326e208",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Fthinking_out_ed.png?alt=media&token=2fdcf6c2-0d75-4634-8339-7fdb5e82d8f2"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FEd%20Sheeran%20-%20Thinking%20Out%20Loud.mp3?alt=media&token=7afce16d-2be6-4f90-8342-ad1156c3b5da",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FEd%20Sheeran%20-%20Thinking%20Out%20Loud.mp4?alt=media&token=60cf1d19-4b03-46e3-a6f2-e1e04bdcdf92",
                null
        ));

        //26. Enrique Iglesias - DUELE EL CORAZON
        songsToUpload.add(new Song(null," Duele El Corazon","Enrique Iglesias",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FEnrique%20Iglesias%20-%20DUELE%20EL%20CORAZON.mp3?alt=media&token=69cd9ca3-1a4f-4f6e-9033-ae794627c5e5",
                null,"vSk_xOy6Bwc",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FEnrique%20Iglesias%20-%20%20Duele%20El%20Corazon.txt?alt=media&token=d77b9cff-1539-466a-9279-877d739f0f52",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FDuele_el_corazon.jpg?alt=media&token=4c785a1c-d904-46ba-bafc-a8bdf61bdd91"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FEnrique%20Iglesias%20-%20DUELE%20EL%20CORAZON%20ft.%20Wisin.mp3?alt=media&token=4d004361-9daf-4c81-abe2-a1ca9e261509",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FEnrique%20Iglesias%20-%20DUELE%20EL%20CORAZON.mp4?alt=media&token=fd9bf1c9-bf44-4761-93a9-5b750078e5fe",
                null
        ));


        //27. Jason Mraz - I Won't Give Up
        songsToUpload.add(new Song(null,"I Won't Give Up","Jason Mraz",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FJason%20Mraz%20-%20I%20Won't%20Give%20Up.mp3?alt=media&token=d026e051-1b7f-4887-adb9-9d47870075c8",
                null,"TdN5GyTl8K0",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FJason%20Mraz%20-%20I%20Wont%20Give%20Up.txt?alt=media&token=fc8b43f6-51ab-4d77-84e2-e11216671dad",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FJason%20Mraz-%20I%20Won't%20Give%20Up.jpg?alt=media&token=8d8fbf9d-a329-4235-a11f-e4c56e613393"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FJason%20Mraz%20-%20I%20Wont%20Give%20Up.mp3?alt=media&token=08b19fbf-8e73-41ce-aa3b-7c24792ced64",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FJason%20Mraz%20-%20I%20Won't%20Give%20Up.mp4?alt=media&token=e04eba0b-9f54-424c-983c-2e40ac513e3f",
                null
        ));

        //28. Justin Timberlake - Cry Me A River
        songsToUpload.add(new Song(null," Cry Me A River","Justin Timberlake",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FJustin%20Timberlake%20-%20Cry%20me%20a%20River.mp3?alt=media&token=d73261b3-7e97-4a07-8890-f40f2bad7a88",
                null,"DksSPZTZES0",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FJustin%20Timberlake%20-%20Cry%20Me%20A%20River.txt?alt=media&token=e1e22e3e-814e-46c7-a99f-9346366f9959",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FJustin%20Timberlake-%20Cry%20Me%20a%20River.jpg?alt=media&token=321810c9-20ad-4331-9d24-1930cdde72d1"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FJustin%20Timberlake%20-%20Cry%20Me%20A%20River.mp3?alt=media&token=1c99b5b4-980a-46be-92c7-9e9c8b40698a",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FJustin%20Timberlake%20-%20Cry%20me%20a%20River.mp4?alt=media&token=7ae6b82d-152a-4e3d-9027-c7e7d1147d98",
                null
        ));

        //29. Justin Timberlake - Rock Your Body
        songsToUpload.add(new Song(null,"Rock Your Body","Justin Timberlake",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FJustin%20Timberlake%20-%20Rock%20Your%20Body.mp3?alt=media&token=1413de80-2f9c-46dc-9500-123575381d2e",
                null,"TSVHoHyErBQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FJustin%20Timberlake%20-%20Rock%20Your%20Body.txt?alt=media&token=2c1748bf-47ac-4270-8139-18b9a8669b25",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FJustin%20Timberlake-%20Rock%20Your%20Body.jpg?alt=media&token=5797d8ac-633b-44fb-abc2-a951524bf0ba"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FJustin%20Timberlake%20-%20Rock%20Your%20Body.mp3?alt=media&token=266fa79e-80fd-4c69-b252-0a34c845d519",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FJustin%20Timberlake%20-%20Rock%20Your%20Body.mp4?alt=media&token=f07f46c1-386b-49cb-862c-d858ff7ab429",
                null
        ));


        //30. Lana Del Rey - Young and Beautiful
        songsToUpload.add(new Song(null,"Young and Beautiful","Lana Del Rey",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FLana%20Del%20Rey%20-%20Young%20and%20Beautiful.mp3?alt=media&token=2370950a-c9d3-4fce-8675-e0627febe353",
                null,"LVHU_YWV3e4",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FLana%20Del%20Rey%20-%20Young%20and%20Beautiful.txt?alt=media&token=e38fd845-e31d-42db-ae5b-53082f2dd066",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FLana%20Del%20Rey-%20Young%20And%20Beautiful.jpg?alt=media&token=e9f442f0-0c83-4efb-b79d-3e935d51df24"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FLana%20del%20rey%20-%20Young%20and%20Beautiful.mp3?alt=media&token=f0e3f3e2-0c0e-4bd0-a783-cf04d4687ef5",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FJustin%20Timberlake%20-%20Rock%20Your%20Body.mp4?alt=media&token=f07f46c1-386b-49cb-862c-d858ff7ab429",
                null
        ));

        //31. Lily Allen - Somewhere Only We Know
        songsToUpload.add(new Song(null,"Somewhere Only We Know"," Lily Allen",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FLily%20Allen%20-%20Somewhere%20Only%20We%20Know.mp3?alt=media&token=0c42a280-ae76-408a-947e-26b242635d9b",
                null,"fAizIX_TNVM",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FLily%20Allen%20-%20Somewhere%20Only%20We%20Know.txt?alt=media&token=fdfb672a-9283-494c-807c-dd8b65f35052",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FLily%20Allen-%20Somewhere%20Only%20We%20Know.jpg?alt=media&token=de640c53-4d29-45a3-8a75-f09a9e706283"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FLily%20Allen%20-%20Somewhere%20Only%20We%20Know.mp3?alt=media&token=2307a289-572e-4094-a637-a0a646949204",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FLily%20Allen%20-%20Somewhere%20Only%20We%20Know.mp4?alt=media&token=85f2bf40-38d1-4794-86ec-fc5e8c0d99b3",
                null
        ));

        //32. Lorde - Yellow Flicker Beat
        songsToUpload.add(new Song(null,"Yellow Flicker Beat","Lorde",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FLorde%20-%20Yellow%20Flicker%20Beat.mp3?alt=media&token=fdb98dcc-a1e7-4906-b8f8-30215de699a5",
                null,"3PdILZ_1P74",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FLorde-%20yellow%20flicker%20beat.txt?alt=media&token=52bcb41a-795c-4dfe-855b-7cb5a859a684",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FLorde%20-%20yellow%20flicker%20beat.jpg?alt=media&token=a39822d3-614d-4453-ba51-9a3ad75f4777"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FLorde%20-%20Yellow%20Flicker%20Beat%20(Hunger%20Games).mp3?alt=media&token=8df0d8d6-d160-4703-ae65-91af5831cc01",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FLorde%20-%20Yellow%20Flicker%20Beat.mp4?alt=media&token=0a20364a-6620-4541-a1f5-583efc3df88d",
                null
        ));

        //33. Louis Armstrong - What a wonderful world
        songsToUpload.add(new Song(null,"What a wonderful world","Louis Armstrong",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FLouis%20Armstrong%20-%20What%20A%20Wonderful%20World.mp3?alt=media&token=f374a22e-1950-436d-8913-3f0857befbba",
                null,"A3yCcXgbKrE",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FLouis%20Armstrong%20-%20What%20a%20Wonderful%20World.txt?alt=media&token=f4672c4b-f1d8-48f3-bfd3-6fd4cd619523",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FLouis%20Armstrong-%20What%20a%20Wonderful%20World.jpg?alt=media&token=df25de42-7854-426c-9960-542324a22ce0"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FLouis%20Armstrong%20-%20What%20a%20wonderful%20world.mp3?alt=media&token=db5d7ac7-a40b-4d1c-8977-f1e86feba1e9",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FLouis%20Armstrong%20-%20What%20A%20Wonderful%20World.mp4?alt=media&token=047a750e-1e2c-4e09-8757-bf8567d16ad4",
                null
        ));

        //34. Madonna - Don't Tell Me
        songsToUpload.add(new Song(null,"Don't Tell Me","Madonna",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMadonna%20-%20Don't%20Tell%20Me.mp3?alt=media&token=8f465137-8db2-4912-9125-72c750c3b43e",
                null,"FRLHro9EPD0",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMadonna%20-%20don't%20tell%20me.txt?alt=media&token=37cfa3dd-c192-4dba-b9b7-50fc3258c02c",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMadonna%20-dont%20tell%20me.jpg?alt=media&token=f0e96bf8-f161-4787-9d4f-a3ba2a3c872a"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMadonna%20-%20Don't%20Tell%20Me.mp3?alt=media&token=c2e528bf-e572-49e0-a354-b45edfa31146",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMadonna%20-%20Don't%20Tell%20Me.mp4?alt=media&token=4927b007-e1fd-4e3b-8d9b-32f476ab64bb",
                null
        ));

        //35. MAGIC! - Rude
        songsToUpload.add(new Song(null,"Rude","MAGIC!",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMAGIC!%20-%20Rude.mp3?alt=media&token=51208944-285b-49e3-9efc-f4861554a39b",
                null,"PIh2xe4jnpk",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMAGIC!%20-%20Rude.txt?alt=media&token=6ecf64f2-550d-454d-b966-c25ebc73c968",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMagic!-%20Rude.jpg?alt=media&token=75550db0-8926-49a2-ad08-4950e057605e"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMAGIC!%20-%20Rude.mp3?alt=media&token=47326584-7cb0-4c6d-bc0a-b7648b1d8ba7",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMAGIC!%20-%20Rude.mp4?alt=media&token=0fc2b64f-1d4c-4a28-96a4-be8c01ef49ff",
                null
        ));

        //36. Mark Ronson - Uptown Funk ft. Bruno Mars
        songsToUpload.add(new Song(null,"Uptown Funk","Mark Ronson ft. Bruno Mars",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMark%20Ronson%20%20ft.%20Bruno%20Mars%20-%20Uptown%20Funk.mp3?alt=media&token=2c2305c6-77cd-429d-9b60-503d1839980f",
                null,"OPf0YbXqDm0",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FBruno%20Mars%20-%20Uptown%20Funk.txt?alt=media&token=4996bd6a-1f29-42f9-9e86-0713be82e99d",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FBruno%20mars-%20uptown%20funk.jpg?alt=media&token=5d5cc3ac-1ece-410a-939f-6ffbd175ff0c"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMark%20Ronson%20-%20Uptown%20Funk%20ft.%20Bruno%20Mars.mp3?alt=media&token=88ab0c3d-df01-48b8-b844-45af1ee25d40",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMark%20Ronson%20%20ft.%20Bruno%20Mars%20-%20Uptown%20Funk.mp4?alt=media&token=91078e7b-535b-4a52-a513-c288083563be",
                null
        ));

        //37. Martin Garrix & Bebe Rexha - In The Name Of Love
        songsToUpload.add(new Song(null,"In The Name Of Love","Martin Garrix & Bebe Rexha",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMartin%20Garrix%20%26%20Bebe%20Rexha%20-%20In%20The%20Name%20Of%20Love.mp3?alt=media&token=28cc1ec4-171f-4dec-9759-915d2e0633a8",
                null,"AeGfss2vsZg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMartin%20Garrix%20%26%20Bebe%20Rexha%20-%20In%20the%20Name%20of%20Love.txt?alt=media&token=a5da8bd3-abdb-46c4-a9c4-42011cdb48d8",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMartin%20Garrix%20%26%20Bebe%20Rexha-%20In%20The%20Nme%20Of%20Love.jpg?alt=media&token=57126015-a581-46b4-ab8c-2da2b5fcf487"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMartin%20Garrix%20%26%20Bebe%20Rexha%20-%20In%20The%20Name%20Of%20Love.mp3?alt=media&token=40d060ef-b502-47a7-8de9-fcba91149eff",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMartin%20Garrix%20%26%20Bebe%20Rexha%20-%20In%20The%20Name%20Of%20Love.mp4?alt=media&token=a42dbd98-17a7-41f7-ac4f-7a4530378c17",
                null
        ));

        //38. Michael Jackson, Justin Timberlake - Love Never Felt So Good
        songsToUpload.add(new Song(null,"Love Never Felt So Good","Michael Jackson, Justin Timberlake",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMichael%20Jackson%20ft.%20Justin%20Timberlake%20-%20Love%20Never%20Felt%20So%20Good.mp3?alt=media&token=07514c14-ae1b-4856-b0f9-2149e27c3375",
                null,"oG08ukJPtR8",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMichael%20Jackson%20%26%20Justin%20Timberlake%20-%20Love%20Never%20Felt%20So%20Good.txt?alt=media&token=10996e9e-6770-40e3-a0cf-91f138417022",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMichael%20Jackson-%20Love%20Never%20Felt%20So%20Good.jpg?alt=media&token=070bb0c8-ecc9-49aa-968d-b237cb6753c3"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMichael%20Jackson%2C%20Justin%20Timberlake%20-%20Love%20Never%20Felt%20So%20Good.mp3?alt=media&token=896e2f6a-7bb8-459e-8198-ba7070c8f17e",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMichael%20Jackson%20ft.%20Justin%20Timberlake%20-%20Love%20Never%20Felt%20So%20Good.mp4?alt=media&token=224592fc-6c1c-478a-bb7c-6b3c5e3e3fd2",
                null
        ));

        //39. Miley Cyrus - Wrecking Ball
        songsToUpload.add(new Song(null,"Wrecking Ball","Miley Cyrus",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMiley%20Cyrus%20-%20Wrecking%20Ball.mp3?alt=media&token=d9b8b97a-806e-4b05-a99d-fd10aebb11c7",
                null,"My2FRPA3Gf8",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMiley%20Cyrus%20-%20Wrecking%20Ball.txt?alt=media&token=11bf97b5-4794-4b61-b68b-1471fcb7f959",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMiley%20Cyrus-%20Wrecking%20Ball.jpg?alt=media&token=344c2b9e-4472-4a7e-94d1-fb5daa61a68c"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMiley%20Cyrus%20-%20Wrecking%20Ball.mp3?alt=media&token=1133e9c0-6813-4a74-a121-f8d2ccabf09c",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMiley%20Cyrus%20-%20Wrecking%20Ball.mp4?alt=media&token=87441928-ab3d-449b-ba1c-8417573ffe87",
                null
        ));


        //40. Milky Chance - Stolen Dance
        songsToUpload.add(new Song(null,"Stolen Dance","Milky Chance",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FMilky%20Chance%20-%20Stolen%20Dance.mp3?alt=media&token=2bb79603-6b1c-4a6d-b358-cf365772ce7e",
                null,"iX-QaNzd-0Y",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FMilky%20Chance%20-%20Stolen%20Dance.txt?alt=media&token=9fd23816-997d-49f2-b463-74f021989ac5",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FMilky%20Chance-%20stolen%20dance.jpg?alt=media&token=b2952fd2-1612-4a17-87ac-2a83498f803a"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FMilky%20Chance%20-%20Stolen%20Dance.mp3?alt=media&token=5cb2432d-f24c-47ad-8380-13ffbde9a89b",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FMilky%20Chance%20-%20Stolen%20Dance.mp4?alt=media&token=c695eaa1-c90f-4b92-801b-0c597989d221",
                null
        ));

        //41. OMI - Cheerleader
        songsToUpload.add(new Song(null,"Cheerleader","OMI",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FOMI%20-%20Cheerleader.mp3?alt=media&token=df9af014-2d1e-4700-bb85-cb5ab71a539e",
                null,"7RHM5HBR33A",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FOMI%20-%20Cheerleader.txt?alt=media&token=1c24dc19-1ee0-4b79-a67a-15ce94e0b929",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Fomi-%20cheerleader.jpg?alt=media&token=e9f24dc8-d2db-449b-91c2-c3a0cd3843b8"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FOmi%20-Cheerleader.mp3?alt=media&token=db26c704-66b5-414c-a0a0-b00fc72a2d01",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FOMI%20-%20Cheerleader.mp4?alt=media&token=e1c91ebc-3b2e-4fb4-b8b0-490a410bf86f",
                null
        ));

        //42. OneRepublic - Counting Stars
        songsToUpload.add(new Song(null,"Counting Stars","OneRepublic",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FOneRepublic%20-%20Counting%20Stars.mp3?alt=media&token=14bca052-4dd0-47a5-ba41-7d8c71b72b90",
                null,"hT_nvWreIhg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FOne%20Republic%20-%20Counting%20stars.txt?alt=media&token=9910a022-e035-45c3-8f47-1a161782a716f",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FOne%20Republic%20-%20Counting%20stars.jpg?alt=media&token=b3cc8030-4c63-45d8-9fee-26992d1eeb33"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FOneRepublic%20-%20Counting%20Stars.mp3?alt=media&token=e147d0a8-085b-41c6-8f5c-4107bd6e20e3",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FOneRepublic%20-%20Counting%20Stars.mp4?alt=media&token=c0d911d4-d52b-4b64-b28a-305099d8a077",
                null
        ));

        //43. Passenger - Let Her Go
        songsToUpload.add(new Song(null," Let Her Go","Passenger",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FPassenger%20-%20Let%20Her%20Go%20By.mp3?alt=media&token=6c2d94c7-f13b-4be6-b128-d647eeaf65c5",
                null,"RBumgq5yVrA",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FPassenger-%20let%20her%20go.txt?alt=media&token=50d95d5a-eaf7-4798-bab0-a2f701802b14",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FPassenger-%20let%20her%20go.jpg?alt=media&token=e6f02ab0-46dd-43c6-9eff-cd24c2e2a8cf"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FPassenger%20-%20Let%20Her%20Go.mp3?alt=media&token=c55f8653-7f8c-4d0d-b1cf-484da312076d",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FPassenger%20-%20Let%20Her%20Go%20By.mp4?alt=media&token=0fe9a569-9b59-4d1e-922b-e39e6ef8a702",
                null
        ));

        //44. Sia Ft. Sean Paul - Cheap Thrills
        songsToUpload.add(new Song(null,"Cheap Thrills ","Sia Ft. Sean Paul",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FSia%20feat%20%20Sean%20Paul%20-%20Cheap%20Thrills.mp3?alt=media&token=28d5f0dc-b4bd-446a-bf7f-fa29d5074ddc",
                null,"6mqbAnrtWHo",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FCheap%20Thrills%20-%20Sia%20feat.%20Sean%20Paul.txt?alt=media&token=8143dc28-424c-41e5-b6f0-4df4fc9cfde7",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FSia-%20Cheap%20Thrills.jpg?alt=media&token=d4b6189c-e91e-43c0-b2ee-a1d95ba36cd9"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FSia%20-%20Cheap%20Thrills%20Ft.%20Sean%20Paul%20%5BLyrics%5D%20-New%202016-.mp3?alt=media&token=a3def6a2-952b-4d20-a91d-9c3ab6d98378",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FSia%20feat%20%20Sean%20Paul%20-%20Cheap%20Thrills.mp4?alt=media&token=da6d3420-3b93-4b6f-b3bb-1d1500a8e62b",
                null
        ));


        //45. Rihanna – Disturbia
        songsToUpload.add(new Song(null,"Disturbia","Rihanna",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FRihanna%20-%20Disturbia.mp3?alt=media&token=5284f917-cba3-45e7-ada4-cfef95f67e09",
                null,"sTKY5GTQ1HQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FRihanna%20-%20Disturbia.txt?alt=media&token=46222bb2-e5ec-486b-a429-cd06d677440b",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FRihanna-%20Disturbia.jpg?alt=media&token=5bd7913a-18e5-4f8c-b84d-7771fd1d1fe1"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FRihanna%20-%20Disturbia.mp3?alt=media&token=44203dec-0c55-4b6c-83b8-f6e51a7d9785",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FRihanna%20-%20Disturbia.mp4?alt=media&token=32100d57-eb71-4b2d-8981-ce839a96e6c7",
                null
        ));

        //46. Taylor Swift - Blank Space
        songsToUpload.add(new Song(null,"Blank Space","Taylor Swift",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FTaylor%20swift%20-%20Blank%20Space.mp3?alt=media&token=dfae06a7-e5d0-439b-b0ed-46fbcc614bdd",
                null,"e-ORhEE9VVg",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FTaylor%20Swift%20-%20Blank%20space.txt?alt=media&token=86066188-fc5a-4aea-94af-484f336e799f",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FTaylor%20Swift%20-%20Blank%20space.jpg?alt=media&token=cfcea2cb-7c74-4be6-96f9-1e48877c6d8e"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FTaylor%20Swift%20-%20Blank%20Space.mp3?alt=media&token=4db70d8d-1589-44f6-94c3-ab11601abf6d",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FTaylor%20swift%20-%20Blank%20Space.mp4?alt=media&token=98445e81-338f-483f-bdeb-8c7b0854afe7",
                null
        ));

        //47. The Chainsmokers - Closer
        songsToUpload.add(new Song(null,"Closer","The Chainsmokers",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FThe%20Chainsmokers%20feat.%20Halsey%20-%20Closer.mp3?alt=media&token=4b0c8bfd-3642-461d-8637-67f69b5d0d0f",
                null,"PT2_F-1esPk",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FThe%20Chainsmokers%20-%20Closer.txt?alt=media&token=551a1c1e-c6cb-4995-ae23-9743d7e65174",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2Fcloser_.jpg?alt=media&token=527a8d34-5243-4eb1-83f6-ff7c45922394"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FThe%20Chainsmokers%20-%20Closer%20ft.%20Halsey.mp3?alt=media&token=f57ed395-7a16-4c70-b0cf-b185841d4ba6",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FThe%20Chainsmokers%20feat.%20Halsey%20-%20Closer.mp4?alt=media&token=04e6f9ac-60c2-4430-abde-fefe65bb332a",
                null
        ));

        //48. U2 - Ordinary Love
        songsToUpload.add(new Song(null,"Ordinary Love","U2",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FU2%20-%20Ordinary%20Love.mp3?alt=media&token=b556ad55-f171-46b7-a16c-4f837f43b29c",
                null,"XC3ahd6Di3M",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FU2%20-%20Ordinary%20Love.txt?alt=media&token=93626723-2915-4129-beac-4adba83ee12d",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FU2%20-%20Ordinary%20Love.jpg?alt=media&token=3d5d8b2d-3032-41b8-b608-34b50870589c"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FU2%20-%20Ordinary%20Love.mp3?alt=media&token=ce1fb22e-e2bb-4d56-b924-bf5002b81721",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FU2%20-%20Ordinary%20Love.mp4?alt=media&token=e6546d78-ac76-4227-82aa-f0efb40a19e1",
                null

        ));

        //49. U2 - With Or Without You
        songsToUpload.add(new Song(null,"With Or Without You","U2",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FU2%20-%20With%20Or%20Without%20You.mp3?alt=media&token=bf2dcec0-831c-4e56-bc25-a7961ba50993",
                null,"XmSdTa9kaiQ",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FU2%20-%20With%20or%20without%20you.txt?alt=media&token=bf2b598c-6c29-42c3-afea-8b7c3a45cfd0",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FU2%20-%20With%20Or%20Without%20You.jpg?alt=media&token=aee5a923-3b89-413d-b674-4922dcd8f0abc"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FU2%20-%20With%20Or%20Without%20You.mp3?alt=media&token=1359adac-4a45-40ee-acde-04ac09349f31",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FU2%20-%20With%20Or%20Without%20You.mp4?alt=media&token=77f07465-afce-4b12-81c9-40c1ae712249",
                null
        ));

        //50. Wiz Khalifa ft. Charlie Puth- See You Again
        songsToUpload.add(new Song(null,"See You Again","Wiz Khalifa ft. Charlie Puth",0,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp3%2FWiz%20Khalifa%20feat.%20Charlie%20Puth%20-%20%20See%20You%20Again.mp3?alt=media&token=0ed56f93-b677-48e2-b685-c17105795b4c",
                null,"RgKAFK5djSk",
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/lyricsTXT%2FWiz%20Khalifa%20-%20See%20You%20Again.txt?alt=media&token=d04b2b82-644d-4546-89b4-1431ffe45805",
                null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/images%2FWiz%20Khalifa%20-%20See%20You%20Again.jpg?alt=media&token=a659cd01-a73d-4033-9d39-f0c4a947e69c"
                ,null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/songs%2FWiz%20Khalifa%20-%20See%20You%20Again%20ft.%20Charlie%20Puth%20.mp3?alt=media&token=ca327de8-0633-4111-be59-f57333d19366",
                null,null,
                "https://firebasestorage.googleapis.com/v0/b/karaoketogo-52e29.appspot.com/o/playbacks%20mp4%2FWiz%20Khalifa%20feat.%20Charlie%20Puth%20-%20%20See%20You%20Again.mp4?alt=media&token=3230aef7-bbad-40bc-a583-ef45a738e39d",
                null
        ));
        //--------------------------------------------------





        for(Song s:songsToUpload)
        {
            HashMap<String, Object> songResult = new HashMap<>();
            songResult.put("name",s.getName());
            songResult.put("artist",s.getArtist());
            songResult.put("popularity",s.getPopularity());
            songResult.put("playBackMp3FileName",s.getPlayBackFileNameRemote());
            songResult.put("songYoutubeUrl",s.getSongYoutubeUrl());
            songResult.put("lyricsTXTFileName",s.getLyricsTXTFileNameRemote());
            songResult.put("imageFileName",s.getImageFileNameRemote());
            songResult.put("fullSongFileName",s.getFullSongFileNameRemote());
            songResult.put("playBackMp4FileName",s.getPlayBackMp4FileNameRemote());

            //the null object will not be entered to the database



            final String songIdFromFirebase=database.getReference("songs").push().getKey();
            s.setSongId(songIdFromFirebase);
            database.getReference("songs").child(songIdFromFirebase).setValue(songResult);
        }


    }

    public void getAllSongs(final Model.GetAllSongsListener listener) {
        DatabaseReference myRef = database.getReference();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChild("songs"))
                {
                    listener.hideProgressBar();
                    return;
                }

                DatabaseReference ref = database.getReference("songs");
                ref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if(dataSnapshot==null)
                        {
                            listener.hideProgressBar();
                            return;
                        }

                        final Song song=new Song();
                        song.setSongId(dataSnapshot.getKey().toString());
                        song.setName(dataSnapshot.child("name").getValue().toString());
                        song.setArtist(dataSnapshot.child("artist").getValue().toString());
                        song.setPopularity(new Integer(String.valueOf(dataSnapshot.child("popularity").getValue())));

                        song.setPlayBackFileNameRemote(dataSnapshot.child("playBackMp3FileName").getValue().toString());
                        song.setSongYoutubeUrl(dataSnapshot.child("songYoutubeUrl").getValue().toString());
                        song.setLyricsTXTFileNameRemote(dataSnapshot.child("lyricsTXTFileName").getValue().toString());
                        song.setFullSongFileNameRemote(dataSnapshot.child("fullSongFileName").getValue().toString());
                        song.setPlayBackMp4FileNameRemote(dataSnapshot.child("playBackMp4FileName").getValue().toString());


                        String absoluteUrl=dataSnapshot.child("imageFileName").getValue().toString();

                        if(!absoluteUrl.equals(""))
                        {
                            song.setImageFileNameRemote(absoluteUrl);
                            Glide.with(MyAppContext.getAppContext())
                                    .load(absoluteUrl)
                                    .asBitmap()
                                    .toBytes()
                                    .centerCrop()
                                    .into(new SimpleTarget<byte[]>(200, 200) {
                                        @Override
                                        public void onResourceReady(byte[] data, GlideAnimation anim) {

                                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                            song.setSongImage(bitmap);
                                            listener.onComplete(song);
                                            listener.hideProgressBar();

                                        }
                                    });

                        }
                        else
                        {
                            song.setSongImage(null);
                            song.setImageFileNameRemote("");
                            listener.onComplete(song);
                            listener.hideProgressBar();


                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void downloadPlaybackAndSrt(final Song song, final Model.DownloadPlaybackAndSrtListener localListner) {

        StorageReference storage=FirebaseStorage.getInstance().getReferenceFromUrl(song.getPlayBackFileNameRemote());

        //writing to internal storage,in order that user won't be able to access it

        String fileName=song.getArtist()+" - "+song.getName()+".mp3";
        File songFile = new File(MyAppContext.getAppContext().getFilesDir(), fileName);
        song.setPlaybackFileNameLocal(songFile.getAbsolutePath());

        //download playback from firebase to file directly
        storage.getFile(songFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("bla","uploaded song succcessfully to file");
                String fileNameRemote=song.getCompletedSrts().get(0).getSRTFileNameRemote();

                StorageReference currentStorage=FirebaseStorage.getInstance().getReferenceFromUrl(fileNameRemote);

                //writing to internal storage,in order that user won't be able to access it

                String fileName=song.getArtist()+" - "+song.getName()+".srt";
                File srtFile = new File(MyAppContext.getAppContext().getFilesDir(), fileName);
                song.getCompletedSrts().get(0).setSRTFileNameLocal(srtFile.getAbsolutePath());

                //download playback from firebase to file directly
                currentStorage.getFile(srtFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d("bla","uploaded song succcessfully to file");

                        localListner.playSongInMediaPlayer(song);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.d("bla","failed to load song to file");
                    }
                });




            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("bla","failed to load song to file");
            }
        });


    }

    public void getAllSongImages(ArrayList<Song> songs, final Model.GetAllSongImagesListener listener) {

        listener.showProgressBar();
        for(final Song s:songs)
        {
            Glide.with(MyAppContext.getAppContext())
                    .load(s.getImageFileNameRemote())
                    .asBitmap()
                    .toBytes()
                    .centerCrop()
                    .into(new SimpleTarget<byte[]>(200, 200) {
                        @Override
                        public void onResourceReady(byte[] data, GlideAnimation anim) {

                            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                            s.setSongImage(bitmap);
                            listener.onComplete(s);
                            listener.hideProgressBar();
                        }
                    });
        }
    }

    public void getSongsTxtAndSong(final Song song, final Model.DownloadTxtAndFullSongListener listener) {
        //downloading txt
        StorageReference storage=FirebaseStorage.getInstance().getReferenceFromUrl(song.getLyricsTXTFileNameRemote());

        String fileName=song.getArtist()+" - "+song.getName()+"_text.txt";
        File songFile = new File(MyAppContext.getAppContext().getFilesDir(), fileName);
        song.setLyricsTXTFileNameLocal(songFile.getAbsolutePath());

        //download playback from firebase to file directly
        storage.getFile(songFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("bla","uploaded song succcessfully to file");


                //downloading full song
                StorageReference storage=FirebaseStorage.getInstance().getReferenceFromUrl(song.getFullSongFileNameRemote());

                String fileName=song.getArtist()+" - "+song.getName()+"_full.mp3";
                File songFile = new File(MyAppContext.getAppContext().getFilesDir(), fileName);
                song.setFullSongFileNameLocal(songFile.getAbsolutePath());

                //download playback from firebase to file directly
                storage.getFile(songFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d("bla","uploaded song succcessfully to file");
                        listener.playSongInMediaPlayerAndHandleTXT(song);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.d("bla","failed to load song to file");
                    }
                });


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("bla","failed to load song to file");
            }
        });
    }

    public void addSrtTimingToDB(final Song song, final Model.addSrtTimingToDBListener listener) {

        final CompletedSrt completedSrt=song.getCompletedSrts().get(0);

        final HashMap<String, Object> completedSrtResult = new HashMap<>();
        completedSrtResult.put("popularity",completedSrt.getPopularity());
        completedSrtResult.put("ownerId",mAuth.getCurrentUser().getUid());



        final String srtId=database.getReference("songs").child(song.getSongId()).child("completedSrt").push().getKey();

        completedSrt.setSrtId(srtId);
//        database.getReference("songs").child(songIdFromFirebase).setValue(songResult);



        Uri file = Uri.fromFile(new File(song.getCompletedSrts().get(0).getSRTFileNameLocal()));
        //FirebaseStorage.getInstance().getReferenceFromUrl
        String nameOfSong=song.getArtist()+" - "+song.getName()+"_"+song.getSongId()+"_"+srtId+".srt";
        StorageReference riversRef = mStorageRef.child("finalSRT/" + nameOfSong );

        UploadTask uploadTask = riversRef.putFile(file);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                listener.hideProgressBar();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                @SuppressWarnings("VisibleForTests") String srtPath = taskSnapshot.getDownloadUrl().toString();
                completedSrt.setSRTFileNameRemote(srtPath);

                completedSrtResult.put("srtFileName",srtPath);


                database.getReference("songs").child(song.getSongId()).child("completedSrt").child(srtId).setValue(completedSrtResult);

                database.getReference("users").child(mAuth.getCurrentUser().getUid()).child("songsOfUser").child(song.getSongId()).child(completedSrt.getSrtId()).setValue(true);


                DatabaseReference myRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());
                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //add points to user when he made the timing
                        long points=(long)dataSnapshot.child("points").getValue();
                        points=points+ Constants.pointsForTimingSong;

                        database.getReference("users").child(mAuth.getCurrentUser().getUid()).child("points").setValue((int)points);
                        listener.hideProgressBar();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        listener.hideProgressBar();
                    }
                });

            }
        });
    }

    public void getUserDetailsAndSongs(final Model.GetUserDetailsAndSongsListener listener) {
        listener.showProgressbar();


        final DatabaseReference myRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final User currentUser=new User();
                currentUser.setFirstName(dataSnapshot.child("firstName").getValue().toString());
                currentUser.setLastName(dataSnapshot.child("lastName").getValue().toString());
                currentUser.setPoints(new Integer(String.valueOf(dataSnapshot.child("points").getValue())));
                listener.addUserDetails(currentUser);
                if(!dataSnapshot.hasChild("songsOfUser"))
                {
                    listener.hideProgressbar();
                    return;
                }

                for(DataSnapshot songIds:dataSnapshot.child("songsOfUser").getChildren())
                {
                    String songId=songIds.getKey().toString();
                    for(DataSnapshot timingIdsOfOneSong:dataSnapshot.child("songsOfUser").child(songIds.getKey().toString()).getChildren())
                    {
                        final String timingId=timingIdsOfOneSong.getKey().toString();
                        DatabaseReference songsRef=database.getReference("songs").child(songId);

                        songsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot==null)
                                {
                                    listener.hideProgressbar();
                                    return;
                                }

                                final Song song=new Song();
                                song.setSongId(dataSnapshot.getKey().toString());
                                song.setName(dataSnapshot.child("name").getValue().toString());
                                song.setArtist(dataSnapshot.child("artist").getValue().toString());
                                song.setPopularity(new Integer(String.valueOf(dataSnapshot.child("popularity").getValue())));
                                song.setPlayBackFileNameRemote(dataSnapshot.child("playBackMp3FileName").getValue().toString());
                                song.setSongYoutubeUrl(dataSnapshot.child("songYoutubeUrl").getValue().toString());
                                song.setLyricsTXTFileNameRemote(dataSnapshot.child("lyricsTXTFileName").getValue().toString());
                                song.setFullSongFileNameRemote(dataSnapshot.child("fullSongFileName").getValue().toString());
                                song.setPlayBackMp4FileNameRemote(dataSnapshot.child("playBackMp4FileName").getValue().toString());

                                CompletedSrt completedSrt=new CompletedSrt(timingId,
                                        dataSnapshot.child("completedSrt").child(timingId).child("srtFileName").getValue().toString(),
                                        null,
                                        new Integer(String.valueOf(dataSnapshot.child("completedSrt").child(timingId).child("popularity").getValue().toString())),
                                        dataSnapshot.child("completedSrt").child(timingId).child("ownerId").getValue().toString(),
                                        null,null);

                                ArrayList<CompletedSrt> al=new ArrayList<CompletedSrt>();
                                al.add(completedSrt);
                                song.setCompletedSrts(al);

                                String absoluteUrl=dataSnapshot.child("imageFileName").getValue().toString();

                                if(!absoluteUrl.equals(""))
                                {
                                    song.setImageFileNameRemote(absoluteUrl);
                                    Glide.with(MyAppContext.getAppContext())
                                            .load(absoluteUrl)
                                            .asBitmap()
                                            .toBytes()
                                            .centerCrop()
                                            .into(new SimpleTarget<byte[]>(200, 200) {
                                                @Override
                                                public void onResourceReady(byte[] data, GlideAnimation anim) {

                                                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                                    song.setSongImage(bitmap);
                                                    listener.addSongToList(song);
                                                    listener.hideProgressbar();

                                                }
                                            });

                                }
                                else
                                {
                                    song.setSongImage(null);
                                    song.setImageFileNameRemote("");
                                    listener.addSongToList(song);
                                    listener.hideProgressbar();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void getAllSrtOfSong(final Song song, final Model.GetAllSrtOfOneSongListener listener) {
        listener.showProgressbar();
        final DatabaseReference myRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot==null)
                {
                    listener.hideProgressbar();
                    return;
                }
                int points= new Integer(String.valueOf(dataSnapshot.child("points").getValue()));
                final DataSnapshot usersSongs=dataSnapshot.child("songsOfUser");

                listener.setPoints(points);

                final DatabaseReference srtRef = database.getReference("songs").child(song.getSongId());
                srtRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot==null)
                        {
                            listener.hideProgressbar();
                            return;
                        }
                        //if there is no completed srts in this song
                        if(!dataSnapshot.hasChild("completedSrt"))
                        {
                            listener.hideProgressbar();
                            return;
                        }

                        DatabaseReference oneSrtRef=database.getReference("songs").child(song.getSongId()).child("completedSrt");
                        oneSrtRef.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                if(dataSnapshot==null)
                                {
                                    listener.hideProgressbar();
                                    return;
                                }
                                if(dataSnapshot.child("ownerId").getValue().toString().equals(mAuth.getCurrentUser().getUid()))
                                {
                                    listener.hideProgressbar();
                                    return;
                                }

                                //--check if this srt is mine..that means i bought it but didn't create it
                                for(DataSnapshot oneSongOfUser:usersSongs.getChildren())
                                {
                                    String songId=new String(oneSongOfUser.getKey().toString());
                                    for(DataSnapshot srt:usersSongs.child(songId).getChildren())
                                    {
                                        String srtId=srt.getKey().toString();
                                        if((song.getSongId().equals(songId)) && (srtId.equals(dataSnapshot.getKey().toString())))
                                        {
                                            listener.hideProgressbar();
                                            return;
                                        }
                                    }
                                }

                                final CompletedSrt cs=new CompletedSrt();
                                cs.setSRTFileNameRemote(dataSnapshot.child("srtFileName").getValue().toString());
                                cs.setSrtId(dataSnapshot.getKey().toString());
                                cs.setOwnerId(dataSnapshot.child("ownerId").getValue().toString());
                                cs.setPopularity(new Integer(String.valueOf(dataSnapshot.child("popularity").getValue())));

                                final DatabaseReference usersRef = database.getReference("users").child(cs.getOwnerId());
                                usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        cs.setOwnerFirstName(dataSnapshot.child("firstName").getValue().toString());
                                        cs.setOwnerLastName(dataSnapshot.child("lastName").getValue().toString());

                                        listener.addSrt(cs);
                                        listener.hideProgressbar();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        listener.hideProgressbar();
                                    }
                                });
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                listener.hideProgressbar();
                                return;
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {
                                listener.hideProgressbar();
                                return;
                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                listener.hideProgressbar();
                                return;
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                listener.hideProgressbar();
                                return;
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        listener.hideProgressbar();
                        return;
                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.hideProgressbar();
            }
        });
    }

    public void buySong(Song song, CompletedSrt currentSrt,int points) {
        database.getReference("users").child(mAuth.getCurrentUser().getUid()).child("points").
                setValue((int)(points-Constants.pointsForSong));

        database.getReference("users").child(mAuth.getCurrentUser().getUid()).child("songsOfUser").child(song.getSongId()).child(currentSrt.getSrtId()).setValue(true);

    }

    public void getPlaybackMp4(final Song song, final Model.GetPlaybackMp4Listener listener) {
        listener.showProgressBar();
        StorageReference storage=FirebaseStorage.getInstance().getReferenceFromUrl(song.getPlayBackMp4FileNameRemote());

        String songName = song.getArtist() + " - " + song.getName() + ".mp4";
        File dest = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songName);


        //writing to internal storage,in order that user won't be able to access it

//        String fileName=song.getArtist()+" - "+song.getName()+"_with_image.mp4";
//        File songFile = new File(MyAppContext.getAppContext().getFilesDir(), fileName);
//        song.setPlaybackMp4FileNameLocal(songFile.getAbsolutePath());

        //download playback from firebase to file directly
        storage.getFile(dest).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("bla","uploaded song succcessfully to file");
                listener.savePlaybackInFile(song);


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("bla","failed to load song to file");
            }
        });
    }
}
