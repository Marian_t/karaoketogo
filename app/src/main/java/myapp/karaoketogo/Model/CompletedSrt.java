package myapp.karaoketogo.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

import java.io.Serializable;

/**
 * Created by Marian on 4/27/2017.
 */

public class CompletedSrt implements Serializable {
    String srtId;

    String SRTFileNameRemote;
    String SRTFileNameLocal;

    int popularity;

    String ownerId;
    String ownerFirstName;
    String ownerLastName;



    public CompletedSrt(String srtId, String SRTFileNameRemote, String SRTFileNameLocal, int popularity,
                        String ownerId, String ownerFirstName, String ownerLastName) {
        this.srtId = srtId;
        this.SRTFileNameRemote = SRTFileNameRemote;
        this.SRTFileNameLocal = SRTFileNameLocal;
        this.popularity = popularity;
        this.ownerId=ownerId;
        this.ownerFirstName=ownerFirstName;
        this.ownerLastName=ownerLastName;
    }

    public CompletedSrt() {
    }
    public CompletedSrt(CompletedSrt cs) {
        this.srtId = cs.getSrtId();
        this.SRTFileNameRemote = cs.getSRTFileNameRemote();
        this.SRTFileNameLocal = cs.getSRTFileNameLocal();
        this.popularity = cs.getPopularity();
        this.ownerId=cs.ownerId;

    }


    public String getSrtId() {
        return srtId;
    }

    public void setSrtId(String srtId) {
        this.srtId = srtId;
    }


    public String getSRTFileNameRemote() {
        return SRTFileNameRemote;
    }

    public void setSRTFileNameRemote(String SRTFileNameRemote) {
        this.SRTFileNameRemote = SRTFileNameRemote;
    }

    public String getSRTFileNameLocal() {
        return SRTFileNameLocal;
    }

    public void setSRTFileNameLocal(String SRTFileNameLocal) {
        this.SRTFileNameLocal = SRTFileNameLocal;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }
}
