package myapp.karaoketogo.Model.Subtitles;

/**
 * Created by Marian on 6/3/2017.
 */

public class FatalParsingException extends Exception {

    private static final long serialVersionUID = 6798827566637277804L;

    private String parsingErrror;

    public FatalParsingException(String parsingError){
        super(parsingError);
        this.parsingErrror = parsingError;
    }

    @Override
    public String getLocalizedMessage(){
        return parsingErrror;
    }

}
