package myapp.karaoketogo.Model.Subtitles;


/**
 * Created by Marian on 6/3/2017.
 */

public class Caption {

    public Style style;
    public Region region;

    public Time start;
    public Time end;

    public String content="";

}