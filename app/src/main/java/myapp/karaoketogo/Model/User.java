package myapp.karaoketogo.Model;

/**
 * Created by Marian on 4/22/2017.
 */

public class User {
    private String userId;
    private String firstName;
    private String lastName;
    private int points;
    private String email;
    private String password;


    public User(String userId, String firstName, String lastName, int points,String email,String password) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.points = points;
        this.email=email;
        this.password=password;
    }

    public User() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
