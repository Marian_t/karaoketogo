package myapp.karaoketogo.Model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Marian on 4/27/2017.
 */

public class Song implements Serializable{
    String songId;//song id
    String name;//the name of the song
    String artist;//song artist
    int popularity;//popularity of the song(how much people buy or download it

    String playBackFileNameRemote;//the place of the playback in firebase storage ---Mp3
    String playbackFileNameLocal;

    String songYoutubeUrl;//the song url from youtube

    String lyricsTXTFileNameRemote;//the place of the lyrics txt in firebase storage
    String lyricsTXTFileNameLocal;

    String imageFileNameRemote;
    String imageFileNameLocal;
    Bitmap songImage;

    String fullSongFileNameRemote;
    String fullSongFileNameLocal;

    ArrayList<CompletedSrt> completedSrts;

    String playBackMp4FileNameRemote;//the place of the playback in firebase storage---Mp4
    String playbackMp4FileNameLocal;



    public Song(String songId, String name, String artist, int popularity,
                String playBackFileNameRemote, String playbackFileNameLocal, String songYoutubeUrl, String lyricsTXTFileNameRemote,
                String lyricsTXTFileNameLocal, String imageFileNameRemote, String imageFileNameLocal, Bitmap songImage, String fullSongFileNameRemote, String fullSongFileNameLocal,
                ArrayList<CompletedSrt> completedVideos, String playBackMp4FileNameRemote, String playbackMp4FileNameLocal) {
        this.songId = songId;
        this.name = name;
        this.artist = artist;
        this.popularity = popularity;
        this.playBackFileNameRemote = playBackFileNameRemote;
        this.playbackFileNameLocal = playbackFileNameLocal;
        this.songYoutubeUrl = songYoutubeUrl;
        this.lyricsTXTFileNameRemote = lyricsTXTFileNameRemote;
        this.lyricsTXTFileNameLocal = lyricsTXTFileNameLocal;
        this.imageFileNameRemote = imageFileNameRemote;
        this.imageFileNameLocal = imageFileNameLocal;
        this.songImage = songImage;
        this.fullSongFileNameRemote = fullSongFileNameRemote;
        this.fullSongFileNameLocal = fullSongFileNameLocal;
        this.completedSrts = completedVideos;
        this.playBackMp4FileNameRemote=playBackMp4FileNameRemote;
        this.playbackMp4FileNameLocal=playbackMp4FileNameLocal;
    }

    public Song() {
    }

    public Song(Song s) {
        this.songId = s.getSongId();
        this.name = s.getName();
        this.artist = s.getArtist();
        this.popularity = s.getPopularity();
        this.playBackFileNameRemote = s.getPlayBackFileNameRemote();
        this.playbackFileNameLocal = s.getPlaybackFileNameLocal();
        this.songYoutubeUrl = s.getSongYoutubeUrl();
        this.lyricsTXTFileNameRemote = s.getLyricsTXTFileNameRemote();
        this.lyricsTXTFileNameLocal = s.getLyricsTXTFileNameLocal();
        this.imageFileNameRemote = s.getImageFileNameRemote();
        this.imageFileNameLocal = s.getImageFileNameLocal();
        this.songImage = s.getSongImage();
        this.fullSongFileNameRemote = s.getFullSongFileNameRemote();
        this.fullSongFileNameLocal = s.getFullSongFileNameLocal();
        this.completedSrts = s.getCompletedSrts();
        this.playBackMp4FileNameRemote=s.getPlayBackMp4FileNameRemote();
        this.playbackMp4FileNameLocal=s.getPlaybackMp4FileNameLocal();
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getPlayBackFileNameRemote() {
        return playBackFileNameRemote;
    }

    public void setPlayBackFileNameRemote(String playBackFileNameRemote) {
        this.playBackFileNameRemote = playBackFileNameRemote;
    }

    public String getPlaybackFileNameLocal() {
        return playbackFileNameLocal;
    }

    public void setPlaybackFileNameLocal(String playbackFileNameLocal) {
        this.playbackFileNameLocal = playbackFileNameLocal;
    }

    public String getSongYoutubeUrl() {
        return songYoutubeUrl;
    }

    public void setSongYoutubeUrl(String songYoutubeUrl) {
        this.songYoutubeUrl = songYoutubeUrl;
    }

    public String getLyricsTXTFileNameRemote() {
        return lyricsTXTFileNameRemote;
    }

    public void setLyricsTXTFileNameRemote(String lyricsTXTFileNameRemote) {
        this.lyricsTXTFileNameRemote = lyricsTXTFileNameRemote;
    }

    public String getLyricsTXTFileNameLocal() {
        return lyricsTXTFileNameLocal;
    }

    public void setLyricsTXTFileNameLocal(String lyricsTXTFileNameLocal) {
        this.lyricsTXTFileNameLocal = lyricsTXTFileNameLocal;
    }

    public String getImageFileNameRemote() {
        return imageFileNameRemote;
    }

    public void setImageFileNameRemote(String imageFileNameRemote) {
        this.imageFileNameRemote = imageFileNameRemote;
    }

    public String getImageFileNameLocal() {
        return imageFileNameLocal;
    }

    public void setImageFileNameLocal(String imageFileNameLocal) {
        this.imageFileNameLocal = imageFileNameLocal;
    }

    public Bitmap getSongImage() {
        return songImage;
    }

    public void setSongImage(Bitmap songImage) {
        this.songImage = songImage;
    }


    public String getFullSongFileNameRemote() {
        return fullSongFileNameRemote;
    }

    public void setFullSongFileNameRemote(String fullSongFileNameRemote) {
        this.fullSongFileNameRemote = fullSongFileNameRemote;
    }

    public String getFullSongFileNameLocal() {
        return fullSongFileNameLocal;
    }

    public void setFullSongFileNameLocal(String fullSongFileNameLocal) {
        this.fullSongFileNameLocal = fullSongFileNameLocal;
    }

    public ArrayList<CompletedSrt> getCompletedSrts() {
        return completedSrts;
    }

    public void setCompletedSrts(ArrayList<CompletedSrt> completedSrts) {
        this.completedSrts = completedSrts;
    }

    public String getPlayBackMp4FileNameRemote() {
        return playBackMp4FileNameRemote;
    }

    public void setPlayBackMp4FileNameRemote(String playBackMp4FileNameRemote) {
        this.playBackMp4FileNameRemote = playBackMp4FileNameRemote;
    }

    public String getPlaybackMp4FileNameLocal() {
        return playbackMp4FileNameLocal;
    }

    public void setPlaybackMp4FileNameLocal(String playbackMp4FileNameLocal) {
        this.playbackMp4FileNameLocal = playbackMp4FileNameLocal;
    }
}
