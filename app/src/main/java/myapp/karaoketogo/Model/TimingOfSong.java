package myapp.karaoketogo.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TimingOfSong {
    private int miliseconds;
    private int seconds;
    private int minutes;
    private int hours;
    private ArrayList<LineOfSong> arrayOfSong;


    public TimingOfSong(){
        arrayOfSong=new ArrayList<LineOfSong>();
        miliseconds =0;
        seconds = 0;
        minutes = 0;
        hours = 0;
    }

    public int getMiliseconds() {
        return miliseconds;
    }

    public void setMiliseconds(int miliseconds) {
        this.miliseconds = miliseconds;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public ArrayList<LineOfSong> getArrayOfSong() {
        return arrayOfSong;
    }

    public void setArrayOfSong(ArrayList<LineOfSong> arrayOfSong) {
        this.arrayOfSong = arrayOfSong;
    }

    public void addToArrayOfSongs(LineOfSong line)
    {
        arrayOfSong.add(line);
    }
    public LineOfSong getElementFromArray(int index)
    {
        return arrayOfSong.get(index);
    }

    // The method if the time of lines was changed (-1) - if the user did not schedule the hole line
    public void isTimeOfLineChanged(){
        int startTime=0;
        int endTime=0;
        HashMap <Integer, String> hashOfLines = new HashMap<>();
        int key=0;
        for(int i=0; i<arrayOfSong.size();i++){
            startTime=getElementFromArray(i).getStartTimeOfLine();
            endTime = getElementFromArray(i).getEndTimeOfLine();
            String line = getElementFromArray(i).getLine();
            if(startTime == -1 && endTime == -1){
                hashOfLines.put(key, line);
                key++;
            }
            else{
                if(hashOfLines.containsValue(line)){

                    getElementFromArray(key+1).setStartTimeOfLine(startTime);
                    getElementFromArray(key+1).setEndTimeOfLine(endTime);
                    arrayOfSong.remove(i);
                    key++;
                }
            }

        }
    }




    // The method get time and check how much millisecond, seconds, minutes and hours we have in this time
    public void timeFromMilisecToSrtTime(int time){
        hours = 0;
        minutes = 0;
        seconds = 0;
        miliseconds = 0;
        if(time >= 3600000){
            hours = time / 3600000;
            time %= 3600000;
            minutes = time / 60000;
            time %= 60000;
            seconds = time / 1000;
            time %= 1000;
            miliseconds = time;
        }
        else if(time < 3600000 && time >= 60000){
            minutes = time / 60000;
            time %= 60000;
            seconds = time / 1000;
            time %= 1000;
            miliseconds = time;
        }
        else if(time < 60000 && time >= 1000){
            seconds = time / 1000;
            time %= 1000;
            miliseconds = time;
        }
        else if(time < 1000){
            miliseconds = time;

        }

    }





}
