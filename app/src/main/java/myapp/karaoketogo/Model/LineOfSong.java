package myapp.karaoketogo.Model;

public class LineOfSong {

    private String line;
    private int startTimeOfLine;
    private int endTimeOfLine;

    public LineOfSong() {
    }

    public LineOfSong(String line, int startTimeOfLine, int endTimeOfLine) {
        this.line = line;
        this.startTimeOfLine = startTimeOfLine;
        this.endTimeOfLine = endTimeOfLine;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public int getStartTimeOfLine() {
        return startTimeOfLine;
    }

    public void setStartTimeOfLine(int startTimeOfLine) {
        this.startTimeOfLine = startTimeOfLine;
    }

    public int getEndTimeOfLine() {
        return endTimeOfLine;
    }

    public void setEndTimeOfLine(int endTimeOfLine) {
        this.endTimeOfLine = endTimeOfLine;
    }



}

