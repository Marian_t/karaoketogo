package myapp.karaoketogo.Model;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;

import myapp.karaoketogo.Dialogs.MyProgressBar;

/**
 * Created by Marian on 4/22/2017.
 */

public class Model {

    ModelFirebase modelFirebase=new ModelFirebase();
    ModelLocal modelLocal=new ModelLocal();

    final private static Model instance = new Model();
    private Model() {}
    public static Model instance(){
        return instance;
    }




    public interface LoginListener{
        /**
         * showing progress bar
         */
        void showProgressBar();

        /**
         * hide progress bar
         */
        void hideProgressBar();

        /**
         * make toast with the text:Authentication failed
         * (the message have to be declared inside activity,that why there are no parameters declared inside firebase class)
         */
        void makeToastAuthFailed();

        /**
         * make toast for verify email in firebase with message declared in firebase class
         * @param msg message to show in the toast
         */
        void makeToastVerifyEmail(String msg);

        /**
         * valide the form of register activity
         * @return true if the form is legit false otherwise
         */
        boolean validateFormInRegister();

        /**
         * valide the form of sighIn activity
         * @return true if the form is legit false otherwise
         */
        boolean validateFormInSignIn();

        /**
         * getting the activity that all the things running inside it
         * @return ActivityMain
         */
        Activity getActivity();

        /**
         * printing to log warning
         * @param tag tag to print
         * @param msg message message to print
         * @param tr the stack trace of warning
         */
        void printToLogWarning(String tag,String msg,Throwable tr);

        /**
         * rinting message to log
         * @param tag tag to print
         * @param msg message to print
         */
        void printToLogMessage(String tag,String msg);

        /**
         * printing to log Exception
         * @param tag tag to print
         * @param msg message message to print
         * @param tr the stack trace of warning
         */
        void printToLogException(String tag,String msg,Throwable tr);

        /**
         * update UI and go to the next activity (Chat Activity)
         */
        void goToKaraokeActivity();

        /**
         * if the registration worked,update the buttons(register button disabled and verify email enabled)
         */
        void updateRegisterActivityIfSuccess();
    }

    public interface saveUserRemote
    {
        void saveUserToRemote(User user);
    }

    public void addUser(User user, final LoginListener listener)
    {
        Model.saveUserRemote sur=new saveUserRemote() {
            @Override
            public void saveUserToRemote(User user) {
                modelFirebase.addUser(user,listener);
            }
        };
        modelFirebase.createAccount(user,listener,sur);

    }

    public void verifyEmail(LoginListener listener)
    {
        modelFirebase.sendEmailVerification(listener);
    }

    public void signInAfterRegister(String email,String password,LoginListener listener)
    {

        modelFirebase.signInAfterRegister(email,password,listener);
    }

    public void signIn(String email,String password,LoginListener listener)
    {

        modelFirebase.signIn(email,password,listener);

    }

    public void checkIfUserAuthonticated(LoginListener loginListener) {
        modelFirebase.checkIfUserAuthonticated(loginListener);
    }
    public interface KaraokeListener
    {
        void goToMainActivity();
        void showProgressBar();
        void hideProgressBar();
    }
    public void signOut(KaraokeListener karaokeListener)
    {
        modelFirebase.signOut(karaokeListener);
    }

    public void uploadSongs()
    {
        modelFirebase.uploadSongs();
    }

    public interface GetAllSongsListener
    {
        void onComplete(Song song);
        Context getAppContext();
        void showProgressBar();
        void hideProgressBar();
    }


    public void getAllSongs(GetAllSongsListener listener)
    {
        modelFirebase.getAllSongs(listener);
    }

    public interface DownloadPlaybackAndSrtListener
    {
        /**
         * after downloading song play it in the player
         * @param song the song to play
         */
        void playSongInMediaPlayer(Song song);

    }

    public void downloadPlaybackAndSrt(Song song,final DownloadPlaybackAndSrtListener listener) {
        modelFirebase.downloadPlaybackAndSrt(song,listener);
    }
    public interface GetPlaybackMp4Listener
    {
        void savePlaybackInFile(Song song);
        void showProgressBar();
        void hideProgressBar();

    }

    public void getPlaybackMp4(Song song,final GetPlaybackMp4Listener listener)
    {
        modelFirebase.getPlaybackMp4(song,listener);
    }

    public interface GetAllSongImagesListener
    {
        void onComplete(Song song);
        void showProgressBar();
        void hideProgressBar();
    }

    public void getAllSongImages(ArrayList<Song> songs,GetAllSongImagesListener listener)
    {
        modelFirebase.getAllSongImages(songs,listener);
    }

    public interface DownloadTxtAndFullSongListener
    {
        /**
         * after downloading song play it in the player
         * @param song the song to play
         */
        void playSongInMediaPlayerAndHandleTXT(Song song);
    }

    public void getSongsTxtAndSong(Song song,DownloadTxtAndFullSongListener listener) {
        modelFirebase.getSongsTxtAndSong(song,listener);
    }

    public interface addSrtTimingToDBListener
    {
        void hideProgressBar();
    }
    public void addSrtTimingToDB(Song song,addSrtTimingToDBListener listener) {
        modelFirebase.addSrtTimingToDB(song,listener);
    }

    public interface GetUserDetailsAndSongsListener
    {
        void addSongToList(Song s);
        void addUserDetails(User user);
        void showProgressbar();
        void hideProgressbar();
    }

    public void getUserDetailsAndSongs(GetUserDetailsAndSongsListener listener) {
        modelFirebase.getUserDetailsAndSongs(listener);
    }
    public interface GetAllSrtOfOneSongListener
    {
        void setPoints(int points);
        void addSrt(CompletedSrt cs);
        void showProgressbar();
        void hideProgressbar();
    }

    public void getAllSrtOfOneSong(Song song,GetAllSrtOfOneSongListener listener) {
        modelFirebase.getAllSrtOfSong(song,listener);
    }



    public void buySong(Song song, CompletedSrt currentSrt,int points) {
        modelFirebase.buySong(song,currentSrt,points);
    }

}
