package myapp.karaoketogo.Model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import myapp.karaoketogo.MyAppContext;


public class TextOfSong {
    String filePath;
    String nameOfSong;
    Song song;
    BufferedWriter bufferedWriter;
    FileOutputStream fileOutput;
    File file;

    static String strHour;
    String strMinutes;
    String strSeconds;
    String strMilliseconds;

    public TextOfSong(Song s) {
        song=s;
        strHour="";
        strMinutes="";
        strSeconds="";
        strMilliseconds="";
    }

    public void initializeFile(){
        //------Marian
        filePath= MyAppContext.getAppContext().getFilesDir().toString();
        //--------------------
        nameOfSong= song.getArtist()+" - "+song.getName();
        file = new File(filePath + nameOfSong + ".srt"); //filepath is being passes through //ioc         //and filename through a method

        if (file.exists()) {
            file.delete(); //you might want to check if delete was successfull
        }
        try {
            file.createNewFile();
            fileOutput = new FileOutputStream(file);

        }catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        catch (IOException e) {

            e.printStackTrace();
        }

    }
    //The method get the time of each line and each word and write the time and the line to srt file
    public void insertSubtitleToSrtFile(TimingOfSong timing) {
        List<String> timeOfSong = timeForSrtFile(timing);
        String line = "";

        int lineNumber = 1, k = 0;
        ArrayList<LineOfSong> arrayOfSong = timing.getArrayOfSong();
        int size = timeOfSong.size();

        try {
            initializeFile();

            bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutput, "UTF8"));
            //bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(MyAppContext.getAppContext().getFilesDir() + nameOfSong + ".srt"), "UTF8"));

            //-----marian-----fill song with the location of srt on internal storage
            CompletedSrt completedSrt=new CompletedSrt(null,null,filePath + nameOfSong + ".srt",0,null,null,null);
            ArrayList<CompletedSrt> al=new ArrayList<>();
            al.add(completedSrt);
            song.setCompletedSrts(al);
            //-----------


            for (int s = 0; s < arrayOfSong.size(); s++) {
                if(k < size){
                    line = arrayOfSong.get(s).getLine();
                    bufferedWriter.write(String.valueOf(lineNumber) + "\n");
                    System.out.println(String.valueOf(lineNumber));
                    System.out.println(timeOfSong.get(k));
                    bufferedWriter.write(timeOfSong.get(k) + "\n");
                    String[] lineArr = line.split(" ");
                    for (int i = 0; i < lineArr.length; i++) {

                        for (int j = 0; j < lineArr.length; j++) {
                            if (i == j) {
                                //The appropriate word will be red when the music reaches that word
                                bufferedWriter.write("<u><font color='red'>" + lineArr[i] + "</font></u> ");
                            } else {

                                bufferedWriter.write(lineArr[j] + " ");
                            }
                        }

                        bufferedWriter.write("\n" + "\n");
                        lineNumber++;
                        k++;
                        if (i < lineArr.length - 1) {
                            bufferedWriter.write(String.valueOf(lineNumber) + "\n");
                            bufferedWriter.write(timeOfSong.get(k) + "\n");
                            System.out.println(String.valueOf(lineNumber));
                            System.out.println(timeOfSong.get(k));
                        }

                    }

                }

            }

            lineNumber = 1;

            bufferedWriter.close();
            fileOutput.close();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }





    public List<String> lengthOfEveryWordInLine(String line, int lineLengthInMilliseconds,
                                                int startTimeOfLineInMilliseconds, TimingOfSong timingOfSong) {
        // set array for the timing of each word in the received line
        List<String> results = new ArrayList<String>();
        // Split the received line to seperate words
        String[] words = line.split(" ");
        // Count the number of letters that are not i/o/u/e/y/a to set total amount of syllables
        int numLettrsInLine = 0;
        for (int i = 0; i < line.length(); i++) {
            // if (line.charAt(i) != ' ')
            if (!(line.charAt(i) == 'i' || line.charAt(i) == 'o' || line.charAt(i) == 'u' || line.charAt(i) == 'e'
                    || line.charAt(i) == 'y' || line.charAt(i) == 'a'))

                numLettrsInLine++;
        }
        // Get the time length of the line
        timingOfSong.timeFromMilisecToSrtTime(startTimeOfLineInMilliseconds);
        int hour = timingOfSong.getHours();
        int minutes = timingOfSong.getMinutes();
        int seconds = timingOfSong.getSeconds();
        int miliseconds = timingOfSong.getMiliseconds();

        // convert the time from int to string to set in the srt file
        checkMiliseconds(hour,minutes, seconds, miliseconds);

        // set the calculated strings as the time for the next word
        String startNextVal = strHour+ ":" + strMinutes + ":" + strSeconds + "," + strMilliseconds;

        String endNextVal = "";
        String result = "";

        // Go through each word and calculate the length of time it needs and its string representation for the srt file
        for (int currWord = 0; currWord < words.length; currWord++) {
            int numLettersInWord = 0;// = words[currWord].length();
            // Calculate word length by counting the syllables used in it
            if ((words[currWord].charAt(0) == 'i' || words[currWord].charAt(0) == 'o'
                    || words[currWord].charAt(0) == 'u' || words[currWord].charAt(0) == 'e'
                    || words[currWord].charAt(0) == 'y' || words[currWord].charAt(0) == 'a')) {
                numLettersInWord++;
            }
            for (int currLetter = 0; currLetter < words[currWord].length(); currLetter++) {
                if (!(words[currWord].charAt(currLetter) == 'i' || words[currWord].charAt(currLetter) == 'o'
                        || words[currWord].charAt(currLetter) == 'u' || words[currWord].charAt(currLetter) == 'e'
                        || words[currWord].charAt(currLetter) == 'y' || words[currWord].charAt(currLetter) == 'a')) {
                    numLettersInWord++;
                }
            }
            // double timeSpanPercentage = numLettersInWord/ numLettrsInLine;
            // Get the percentage of time from total syllables in line to the syllables in the current processed word
            float percent = (numLettersInWord * 1.0f) / numLettrsInLine;

            // Calculate words' duration by the total line time and the percentage the word should get by its syllables amount
            float nextDuration = lineLengthInMilliseconds * percent;
            // Set new start time for the next word to include end time of current word
            startTimeOfLineInMilliseconds += nextDuration - 1;

            // Calculate the time of the end of the word
            timingOfSong.timeFromMilisecToSrtTime(startTimeOfLineInMilliseconds);

            hour = timingOfSong.getHours();
            minutes = timingOfSong.getMinutes();
            seconds = timingOfSong.getSeconds();
            miliseconds = timingOfSong.getMiliseconds();

            // convert the time from int to string to set in the srt
            checkMiliseconds(hour,minutes, seconds, miliseconds);
            endNextVal = strHour+ ":" + strMinutes + ":" + strSeconds + "," + strMilliseconds;

            // Set the calculated line as the timing of current word in the results array
            result = startNextVal + " --> " + endNextVal;
            results.add(result);

            // Add a millisecond so words dont clash in the srt file
            startTimeOfLineInMilliseconds += 1;

            // Calculate again the start of the next word in the line
            timingOfSong.timeFromMilisecToSrtTime(startTimeOfLineInMilliseconds);

            hour = timingOfSong.getHours();
            minutes = timingOfSong.getMinutes();
            seconds = timingOfSong.getSeconds();
            miliseconds = timingOfSong.getMiliseconds();

            // convert the time from int to string to set in the srt file
            checkMiliseconds(hour,minutes, seconds, miliseconds);

            // Set next value beginning string and loop the next word
            startNextVal = strHour+ ":" + strMinutes + ":" + strSeconds + "," + strMilliseconds;
        }

        return results;
    }

    //
    public List<String> timeForSrtFile(TimingOfSong timingOfSong) {
        int startTime = 0, endTime = 0;
        String line = "";
        List<String> result = new ArrayList<String>();

        ArrayList<LineOfSong> arrayOfSong = timingOfSong.getArrayOfSong();
        for (int i = 0; i < arrayOfSong.size(); i++) {
            startTime = arrayOfSong.get(i).getStartTimeOfLine();
            endTime = arrayOfSong.get(i).getEndTimeOfLine();
            line = arrayOfSong.get(i).getLine();

            //the user did not schedule the hole line, so we need to stop and will represent on the video only the lines before this line
            if ((endTime==-1)||(startTime == -1 && endTime == -1))
            {
                break;
            }
            else{
                //calculate the total time of line
                int totalTime = endTime - startTime;
                //returns the time of each row divided by the time of each word
                List<String> timeOfWordsInLine = lengthOfEveryWordInLine(line, totalTime, startTime, timingOfSong);
                for (int j = 0; j < timeOfWordsInLine.size(); j++) {
                    String temp = timeOfWordsInLine.get(j);
                    result.add(temp);
                }
            }


        }

        return result;
    }
    //The method check the Millisecond, seconds, minutes and hours and change them to string value
    private void checkMiliseconds(int hour, int minutes , int seconds ,int miliseconds){
        strHour = hour >= 10 ? String.valueOf(minutes) : "0" + String.valueOf(hour);
        strMinutes = minutes >= 10 ? String.valueOf(minutes) : "0" + String.valueOf(minutes);
        strSeconds = seconds >= 10 ? String.valueOf(seconds) : "0" + String.valueOf(seconds);

        if (miliseconds >= 100) {
            strMilliseconds = String.valueOf(miliseconds);
        } else if (miliseconds < 10) {
            strMilliseconds = "00" + String.valueOf(miliseconds);
        } else {
            strMilliseconds = "0" + String.valueOf(miliseconds);
        }
    }
}