package myapp.karaoketogo;

import android.app.Application;
import android.content.Context;

/**
 * Created by Marian on 4/22/2017.
 */

public class MyAppContext extends Application {
    private static Context context;
    public void onCreate() {
        super.onCreate();
        MyAppContext.context = getApplicationContext();
    }
    public static Context getAppContext() {
        return MyAppContext.context;
    }
}
