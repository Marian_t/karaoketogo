package myapp.karaoketogo.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import myapp.karaoketogo.Constants;
import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class BaseMenuActivity extends Activity {


    MyProgressBar progressBar;
    int workingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_menu);
        progressBar=new MyProgressBar(this);
        if(this.getClass()==SearchActivity.class)
        {
            workingActivity= Constants.SEARCH_ACTIVITY;
        }
        else if(this.getClass()==ManagerActivity.class)
        {
            workingActivity=Constants.MANAGER_ACTIVITY;
        }
        else if(this.getClass()==Mysongs.class)
        {
            workingActivity=Constants.MY_SONGS_ACTIVITY;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_bar_sign_out:
                Model.instance().signOut(new Model.KaraokeListener() {
                    @Override
                    public void goToMainActivity() {
                        Intent intent = new Intent(MyAppContext.getAppContext(), MainActivity.class);
                        finish();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }

                    @Override
                    public void showProgressBar() {
                        progressBar.showProgressDialog();
                    }

                    @Override
                    public void hideProgressBar() {
                        progressBar.hideProgressDialog();
                    }
                });
                return true;
            case R.id.menu_bar_all_songs:
                if(workingActivity!=Constants.SEARCH_ACTIVITY)
                {
                    Intent intent1 = new Intent(MyAppContext.getAppContext(), SearchActivity.class);
                    finish();
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);


//                    Intent intent1 = new Intent(MyAppContext.getAppContext(), SearchActivity.class);
//                    startActivity(intent1);
//                    finish();


                }
                return true;
            case R.id.menu_bar_my_songs:
                if(workingActivity!=Constants.MY_SONGS_ACTIVITY)
                {
                    Intent intent3 = new Intent(MyAppContext.getAppContext(), Mysongs.class);
                    finish();
                    intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent3);


                }
                return true;
            case R.id.menu_bar_manager:
                if(workingActivity!=Constants.MANAGER_ACTIVITY)
                {
                    Intent intent2 = new Intent(MyAppContext.getAppContext(), ManagerActivity.class);
                    finish();
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent2);

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
