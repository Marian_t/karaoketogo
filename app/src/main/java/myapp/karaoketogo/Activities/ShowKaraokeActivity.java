package myapp.karaoketogo.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;

import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.Model.Subtitles.Caption;
import myapp.karaoketogo.Model.Subtitles.FormatSRT;
import myapp.karaoketogo.Model.Subtitles.TimedTextObject;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class ShowKaraokeActivity extends BaseMenuActivity {
    private static final String TAG = "TimedTextTest";
    private TextView txtDisplay;
    //private TextView txtDisplay2;
    //private TextView txtDisplay3;
    private static Handler handler = new Handler();
    //private String st = "";
    //private String st1 = "";
    //private String st2 = "";
    //private String st3 = "";
    //private int po = 0;

    TextView currentTime;
    TextView totalTime;

    Runnable runnable;
    SeekBar seekBar;

    MediaPlayer player;
    int playerState;//1-nothing,2-playing,3-was stopped,4-was paused

    MyProgressBar progressBar;
    ImageButton playButton;
    ImageButton stopButton;
    ImageButton pauseButton;
    Button sendEmail;
    Song song;

    Button mySongsBtn;
    Button allSongsBtn;

    //----------subtitles---------------

    private SubtitleProcessingTask subsFetchTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_karaoke);
        progressBar = new MyProgressBar(this);
        playerState = 1;

        seekBar = (SeekBar) findViewById(R.id.SeekBar);
        currentTime = (TextView) findViewById(R.id.activity_build_karaoke_currentTime_TV);
        totalTime = (TextView) findViewById(R.id.activity_build_karaoke_totalTime_TV);

        playButton = (ImageButton) findViewById(R.id.activity_show_karaoke_play);
        stopButton = (ImageButton) findViewById(R.id.activity_show_karaoke_stop);
        pauseButton = (ImageButton) findViewById(R.id.activity_show_karaoke_pause);
        sendEmail = (Button) findViewById(R.id.activity_show_karaoke_sendEmail_btn);
        mySongsBtn = (Button) findViewById(R.id.activity_show_karaoke_goToMySong_btn);
        allSongsBtn = (Button) findViewById(R.id.activity_show_karaoke_goToAllSongs_btn);

        mySongsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUp();
                Intent intent2 = new Intent(MyAppContext.getAppContext(), Mysongs.class);
                finish();
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);

            }
        });
        allSongsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUp();
                Intent intent3 = new Intent(MyAppContext.getAppContext(), SearchActivity.class);
                finish();
                intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent3);

            }
        });

        txtDisplay = (TextView) findViewById(R.id.txtDisplay);
        //txtDisplay2 = (TextView) findViewById(R.id.txtDisplay2);
        //txtDisplay2.setPaintFlags(txtDisplay.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //txtDisplay3 = (TextView) findViewById(R.id.txtDisplay3);


        Intent i = getIntent();
        song = (Song) i.getSerializableExtra("song");
        progressBar.showProgressDialog();
        player = new MediaPlayer();
        Model.instance().downloadPlaybackAndSrt(song, new Model.DownloadPlaybackAndSrtListener() {
            @Override
            public void playSongInMediaPlayer(Song song) {

                String playbakFilePath = song.getPlaybackFileNameLocal();
                String srtFilePath = song.getCompletedSrts().get(0).getSRTFileNameLocal();

                //player = MediaPlayer.create(ShowKaraokeActivity.this, R.raw.fixy_ou);

                try {
                    //play the file from the path of the song
                    player.setDataSource(playbakFilePath);
                    player.addTimedTextSource(srtFilePath, MediaPlayer.MEDIA_MIMETYPE_TEXT_SUBRIP);
                    int textTrackIndex;
                    textTrackIndex = findTrackIndexFor(MediaPlayer.TrackInfo.MEDIA_TRACK_TYPE_TIMEDTEXT, player.getTrackInfo());
                    if (textTrackIndex >= 0) {
                        player.selectTrack(textTrackIndex);
                    } else {
                        Log.w(TAG, "Cannot find text track!");
                    }
                    //player.setOnTimedTextListener(ShowKaraokeActivity.this);
                    //player.prepare();
                    //player.start();
                    progressBar.hideProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        //---------------subtitles-------------------

        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                if(subsFetchTask!=null)
                {
                    subsFetchTask = new SubtitleProcessingTask();
                    subsFetchTask.execute();
                }

                if(!player.isPlaying())
                {
                    player.start();
                }

                //handler.sendEmptyMessage(SHOW_PROGRESS);
            }
        });
        //--------------------------------------------


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if (input) {
                    if (player != null && player.isPlaying()) {
                        player.seekTo(progress);

                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                cleanUp();
            }
        });


        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(player==null)
//                {
//                    player= MediaPlayer.create(ShowKaraokeActivity.this,R.raw.fix_you_song);
//                    try {
//                        player.prepare();
//                        player.start();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else
//                {
//                    player.start();
//
//                }
                if(handler==null)
                {
                    handler=new Handler();
                }


                subsFetchTask = new SubtitleProcessingTask();
                subsFetchTask.execute();

                //1-nothing,2-playing,3-was stopped,4-was paused
                try {
                    if ((player == null) && (playerState == 3))//if player was stopped or played the first time
                    {
                        player = new MediaPlayer();
                        player.setDataSource(song.getPlaybackFileNameLocal());

                        player.addTimedTextSource(song.getCompletedSrts().get(0).getSRTFileNameLocal(), MediaPlayer.MEDIA_MIMETYPE_TEXT_SUBRIP);
                        int textTrackIndex;
                        textTrackIndex = findTrackIndexFor(MediaPlayer.TrackInfo.MEDIA_TRACK_TYPE_TIMEDTEXT, player.getTrackInfo());
                        if (textTrackIndex >= 0) {
                            player.selectTrack(textTrackIndex);
                        } else {
                            Log.w(TAG, "Cannot find text track!");
                        }
                        //player.setOnTimedTextListener(ShowKaraokeActivity.this);
                        player.prepare();
                        player.start();

                    }
                    else if (playerState == 4) //4-was paused
                    {
                        player.start();
                    }
                    else if (playerState == 1)// 1-nothing
                    {
                        player.prepare();
                        player.start();
                        totalTime.setText(getTimeString(player.getDuration()));
                    }
                    seekBar.setMax(player.getDuration());

                    playCycle();





                } catch (IOException e) {
                    e.printStackTrace();
                }


                playerState = 2;


            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player!=null && player.isPlaying()) {
                    player.release();
                    if(handler!=null)
                    {
                        handler.removeCallbacks(runnable);
                        handler.removeCallbacks(subtitleProcessesor);
                        handler=null;
                        if (subsFetchTask != null)
                            subsFetchTask.cancel(true);
                    }
                    player = null;
                    playerState = 3;
                }



                txtDisplay.setText("");
                //txtDisplay2.setText("");
                //txtDisplay3.setText("");

            }
        });
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(handler==null)
                {
                    handler=new Handler();
                }
                if(subsFetchTask==null)
                {
                    subsFetchTask = new SubtitleProcessingTask();
                    subsFetchTask.execute();
                }

                if (player!=null && player.isPlaying()) {
                    player.pause();
                    playerState = 4;
                }

            }
        });
        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateEMail();
            }
        });
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }

    //----------------------subs--------------------
    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        if (player != null && player.isPlaying()) {
            player.pause();
            playerState = 4;

        }
        if (handler != null) {
            handler.removeCallbacks(subtitleProcessesor);
            handler.removeCallbacks(runnable);
            handler = null;
        }

    }
    private void cleanUp() {
        if (handler != null) {
            handler.removeCallbacks(subtitleProcessesor);
            handler.removeCallbacks(runnable);
        }
        if (subsFetchTask != null)
            subsFetchTask.cancel(true);

        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }

    //----------------------------------


    protected void CreateEMail() {
        //------send playback
        //first copy song from internal storage to external downloads directory
        Model.instance().getPlaybackMp4(song, new Model.GetPlaybackMp4Listener() {
            @Override
            public void savePlaybackInFile(Song song) {
                //mp4 was saved in the modelFirebase ,now lets save the srt
                String songName = song.getArtist() + " - " + song.getName() + ".mp4";
//                File src = new File(song.getPlaybackMp4FileNameLocal());
//                File dest = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songName);
//                try {
//                    copyFile(src, dest);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                //send song from downloads
//                File songFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), songName);
//
                //ArrayList<Uri> uris = new ArrayList<Uri>();
//
//                Uri songPath = Uri.fromFile(songFile);
//                uris.add(songPath);


                //--------------send srt
                //first copy song from internal storage to external downloads directory
                String srtName = song.getArtist() + " - " + song.getName() + ".srt";
                File srcSrt = new File(song.getCompletedSrts().get(0).getSRTFileNameLocal());
                File destSrt = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), srtName);
                try {
                    copyFile(srcSrt, destSrt);
                    progressBar.hideProgressDialog();
                } catch (IOException e) {
                    e.printStackTrace();
                    progressBar.hideProgressDialog();
                }

                //send song from downloads
                //File srtFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), srtName);

                //Uri srtPath = Uri.fromFile(srtFile);
                //uris.add(srtPath);

                //android.content.Intent.ACTION_SEND_MULTIPLE
                Intent emailIntent = new Intent(Intent.ACTION_SEND);//send multiple files

                //emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.email_text));

                emailIntent.setType("vnd.android.cursor.dir/email");

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "How to play song: " + songName);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));

            }

            @Override
            public void showProgressBar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressBar() {
                progressBar.hideProgressDialog();
            }
        });

    }


    int findTrackIndexFor(int mediaTrackType, MediaPlayer.TrackInfo[] trackInfo) {
        int index = -1;
        for (int i = 0; i < trackInfo.length; i++) {
            if (trackInfo[i].getTrackType() == mediaTrackType) {
                return i;
            }
        }
        return index;
    }

    private String getSubtitleFile(int resId) {
        String fileName = getResources().getResourceEntryName(resId);
        File subtitleFile = getFileStreamPath(fileName);
        if (subtitleFile.exists()) {
            Log.d(TAG, "Subtitle already exists");
            return subtitleFile.getAbsolutePath();
        }
        Log.d(TAG, "Subtitle does not exists, copy it from res/raw");

        // Copy the file from the res/raw folder to your app folder on the
        // device
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = getResources().openRawResource(resId);
            outputStream = new FileOutputStream(subtitleFile, false);
            copyFile(inputStream, outputStream);
            return subtitleFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStreams(inputStream, outputStream);
        }
        return "";
    }

    private void copyFile(InputStream inputStream, OutputStream outputStream)
            throws IOException, IOException {
        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        int length = -1;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }
    }

    // A handy method I use to close all the streams
    private void closeStreams(InputStream inputStream, OutputStream outputStream, Closeable... closeables) {
        if (closeables != null) {
            for (Closeable stream : closeables) {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

//    @Override
//    public void onTimedText(final MediaPlayer mp, final TimedText text) {
//        if (text != null) {
//            st = "";
//            st2 = "";
//            st1 = "";
//            st3 = "";
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    st = text.getText();
//                    for (int i = 0; i < st.length(); i++) {
//                        if (st.charAt(i) == '<') {
//
//                            po = i;
//                            break;
//                        } else {
//                            st1 = st1 + st.charAt(i);
//                        }
//                    }
//
//                    for (int i = po + 21; i < st.length(); i++) {
//                        if (st.charAt(i) == '<') {
//
//                            po = i;
//                            break;
//                        } else {
//                            st2 = st2 + st.charAt(i);
//                        }
//                    }
//                    for (int i = po + 11; i < st.length(); i++) {
//
//                        st3 = st3 + st.charAt(i);
//
//                    }
//
//                    txtDisplay.setText(st1);
//                    txtDisplay2.setText(st2);
//                    txtDisplay3.setText(st3);
//                }
//            });
//        }
//    }

    public void playCycle() {

        if (player != null && player.isPlaying()) {
            currentTime.setText(getTimeString(player.getCurrentPosition()));
            seekBar.setProgress(player.getCurrentPosition());
            runnable = new Runnable() {
                @Override
                public void run() {
                    playCycle();

                }
            };
            handler.postDelayed(runnable, 1000);
        }
    }

    private String getTimeString(int millis) {
        StringBuffer buf = new StringBuffer();

        int minutes = (millis % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = ((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000;

        buf
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanUp();

    }


    //----------------------subtitles-----------------

    public class SubtitleProcessingTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            try {


                //InputStream stream = getResources().openRawResource(
                   //     R.raw.titnic_4);
                InputStream stream = new FileInputStream(song.getCompletedSrts().get(0).getSRTFileNameLocal());
                FormatSRT formatSRT = new FormatSRT();
                srt = formatSRT.parseFile("sample.srt", stream);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "error in downloadinf subs");
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            if (null != srt) {
                txtDisplay.setText("");
                //Toast.makeText(getApplicationContext(), "subtitles loaded!!",
                        //Toast.LENGTH_SHORT).show();
                handler.post(subtitleProcessesor);
            }
            super.onPostExecute(result);
        }
    }

    public TimedTextObject srt;
    private Runnable subtitleProcessesor = new Runnable() {

        @Override
        public void run() {
            if (player != null && player.isPlaying()) {
                int currentPos = player.getCurrentPosition();
                Collection<Caption> subtitles = srt.captions.values();
                for (Caption caption : subtitles) {
                    if (currentPos >= caption.start.mseconds
                            && currentPos <= caption.end.mseconds) {
                        onTimedText(caption);
                        break;
                    } else if (currentPos > caption.end.mseconds) {
                        onTimedText(null);
                    }
                }
            }
            handler.postDelayed(this, 100);
        }
    };


    public void onTimedText(Caption text) {
        if (text == null) {
            txtDisplay.setVisibility(View.INVISIBLE);
            return;
        }
        txtDisplay.setText(Html.fromHtml(text.content));
        txtDisplay.setVisibility(View.VISIBLE);
    }

}

