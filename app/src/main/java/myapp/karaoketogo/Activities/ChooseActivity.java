package myapp.karaoketogo.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class ChooseActivity extends BaseMenuActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Intent i = getIntent();
        final Song song = (Song)i.getSerializableExtra("song");

        Button diyButton=(Button)findViewById(R.id.activity_choose_diy_btn);
        Button buyButton=(Button)findViewById(R.id.activity_choose_buy_btn);

        diyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAppContext.getAppContext(),BuildKaraokeActivity.class);
                song.setSongImage(null);
                intent.putExtra("song",song);
                startActivity(intent);

            }
        });
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAppContext.getAppContext(),BuyKaraokeActivity.class);
                song.setSongImage(null);
                intent.putExtra("song",song);
                startActivity(intent);

            }
        });
    }

}
