package myapp.karaoketogo.Activities;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.R;

public class ManagerActivity extends BaseMenuActivity{

    Button uploadSongs;
    Button deleteSongs;
    Button deleteUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);

        uploadSongs=(Button)findViewById(R.id.activity_manager_upload_songs);
        deleteSongs=(Button)findViewById(R.id.activity_manager_delete_songs);
        deleteUsers=(Button)findViewById(R.id.activity_manager_delete_users);

        uploadSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Model.instance().uploadSongs();
            }
        });
    }

}
