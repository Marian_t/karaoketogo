package myapp.karaoketogo.Activities;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.User;
import myapp.karaoketogo.R;
import myapp.karaoketogo.Fragments.MainFragment;
import myapp.karaoketogo.Fragments.RegisterFragment;
import myapp.karaoketogo.Fragments.SignInFragment;

public class MainActivity extends Activity {
    private FragmentTransaction ftr;
    private MainFragment mainFragment;
    private RegisterFragment registerFragment;
    private SignInFragment signInFragment;

    private Model.LoginListener loginListener;
    private MyProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar=new MyProgressBar(this);

        //constracting the fragments in this activity
        mainFragment=new MainFragment();
        signInFragment=new SignInFragment();
        registerFragment=new RegisterFragment();

        //display the fragment using fragment manager
        ftr = getFragmentManager().beginTransaction();
        //add to the screen
        ftr.add(R.id.main_container,mainFragment);

        //show fragment
        ftr.show(mainFragment);
        ftr.commit();



        loginListener= new Model.LoginListener() {
            @Override
            public void showProgressBar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressBar() {
                progressBar.hideProgressDialog();
            }

            @Override
            public void makeToastAuthFailed() {
                Toast.makeText(MainActivity.this, R.string.auth_failed,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void makeToastVerifyEmail(String msg)
            {
                Toast.makeText(MainActivity.this,msg,Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean validateFormInRegister() {
                return registerFragment.validateForm();
            }

            @Override
            public boolean validateFormInSignIn() {
                return signInFragment.validateForm();
            }


            @Override
            public Activity getActivity() {
                return MainActivity.this;
            }

            @Override
            public void printToLogWarning(String tag, String msg, Throwable tr) {
                Log.w(tag, msg,tr);
            }

            @Override
            public void printToLogMessage(String tag, String msg) {
                Log.d(tag,msg);
            }

            @Override
            public void printToLogException(String tag, String msg, Throwable tr) {
                Log.e(tag,msg,tr);
            }

            @Override
            public void goToKaraokeActivity() {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void updateRegisterActivityIfSuccess() {
                registerFragment.enableOrDisableButtons(true,true);
                registerFragment.enableAllTextFields(false);
                registerFragment.changeRegisterButtonText();
            }

        };
        //if a user is already authenticated sign him.
        Model.instance().checkIfUserAuthonticated(loginListener);

        mainFragment.setDelegate(new MainFragment.Delegate() {
            @Override
            public void onSignInPressed() {
                ftr = getFragmentManager().beginTransaction();

                //add to the screen
                ftr.add(R.id.main_container,signInFragment);
                ftr.hide(mainFragment);
                ftr.show(signInFragment);
                ftr.addToBackStack("main");
                ftr.commit();
            }

            @Override
            public void onRegisterPressed() {
                ftr = getFragmentManager().beginTransaction();

                //add to the screen
                ftr.add(R.id.main_container,registerFragment);
                ftr.hide(mainFragment);
                ftr.show(registerFragment);
                ftr.addToBackStack("main");
                ftr.commit();
            }
        });

        registerFragment.setDelegate(new RegisterFragment.Delegate() {

            @Override
            public void onRegisterButtonClick(User user) {
                if(registerFragment.getRegisterBtnTag().equals("1"))
                {
                    Model.instance().addUser(user,loginListener);
                }
                else
                {
                    Model.instance().signInAfterRegister(user.getEmail(),user.getPassword(),loginListener);
                }


            }

            @Override
            public void onVerifyEmailClick(final User user) {
                Model.instance().verifyEmail(loginListener);

            }
        });

        signInFragment.setDelegate(new SignInFragment.Delegate() {
            @Override
            public void onSignInPressed(String email, String password) {
                Model.instance().signIn(email,password,loginListener);
            }
        });
    }
}
