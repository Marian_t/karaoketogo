package myapp.karaoketogo.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;

import myapp.karaoketogo.Constants;
import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener
{

    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private YouTubePlayer player;

    Song song;
    ArrayList<Song> data;
    CustomListAdapter songsAdapter;
    ListView listView;

    Button button;
    MyProgressBar progressBar;

    int workingActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);
        progressBar=new MyProgressBar(this);

        workingActivity=Constants.OTHER_ACTIVITY;

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.YTView);
        youTubePlayerView.initialize(Constants.YoutubeKey,this);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.YTView);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        song=(Song)bundle.getSerializable("songInPosition");//song without bitmap
        data = (ArrayList<Song>) bundle.getSerializable("searchSuggestions");

        ArrayList<Song> emptyData=new ArrayList<>();
        listView= (ListView) findViewById(R.id.activity_youtube_listView);
        songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), emptyData);
        listView.setAdapter(songsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Song songInPosition = (Song) listView.getItemAtPosition(position);
                song=songInPosition;
                player.cueVideo(song.getSongYoutubeUrl());

            }
        });

        Model.instance().getAllSongImages(data, new Model.GetAllSongImagesListener() {
            @Override
            public void onComplete(Song song) {
                songsAdapter.add(song);
            }

            @Override
            public void showProgressBar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressBar() {
                progressBar.hideProgressDialog();
            }
        });






        button=(Button)findViewById(R.id.activity_youtube_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAppContext.getAppContext(),ChooseActivity.class);
                song.setSongImage(null);
                intent.putExtra("song",song);
                startActivity(intent);

            }
        });

        player=null;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b){
            youTubePlayer.cueVideo(song.getSongYoutubeUrl());
            player=youTubePlayer;
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this,"Failured to Initialize", Toast.LENGTH_LONG).show();

    }

    //-------------menu bar----------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_bar_sign_out:
                Model.instance().signOut(new Model.KaraokeListener() {
                    @Override
                    public void goToMainActivity() {
                        Intent intent = new Intent(MyAppContext.getAppContext(), MainActivity.class);
                        finish();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }

                    @Override
                    public void showProgressBar() {
                        progressBar.showProgressDialog();
                    }

                    @Override
                    public void hideProgressBar() {
                        progressBar.hideProgressDialog();
                    }
                });
                return true;
            case R.id.menu_bar_all_songs:
                if(workingActivity!=Constants.SEARCH_ACTIVITY)
                {
                    Intent intent1 = new Intent(MyAppContext.getAppContext(), SearchActivity.class);
                    finish();
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);

                }
                return true;
            case R.id.menu_bar_my_songs:
                if(workingActivity!=Constants.MY_SONGS_ACTIVITY)
                {
                    Intent intent3 = new Intent(MyAppContext.getAppContext(), Mysongs.class);
                    finish();
                    intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent3);


                }
                return true;
            case R.id.menu_bar_manager:
                if(workingActivity!=Constants.MANAGER_ACTIVITY)
                {
                    Intent intent2 = new Intent(MyAppContext.getAppContext(), ManagerActivity.class);
                    finish();
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent2);

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    //-------------------------------------------


    public class CustomListAdapter extends BaseAdapter {
        private ArrayList listData;
        private LayoutInflater layoutInflater;

        public CustomListAdapter(Context context, ArrayList listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.song_list_row, null);
                holder = new ViewHolder();
                holder.songImage= (ImageView) convertView.findViewById(R.id.song_list_row_image_IV);
                holder.artist= (TextView) convertView.findViewById(R.id.song_list_row_artist_TV);
                holder.name = (TextView) convertView.findViewById(R.id.song_list_row_name_TV);
                holder.songId=(TextView)convertView.findViewById(R.id.song_list_row_id_TV);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if(listData==null)
            {
                return convertView;
            }
            Song songInPosition = (Song) listData.get(position);
            holder.name.setText(songInPosition.getName());
            holder.artist.setText(songInPosition.getArtist());
            holder.songImage.setImageBitmap(songInPosition.getSongImage());
            holder.songId.setText(songInPosition.getSongId());

            return convertView;
        }

        class ViewHolder {
            ImageView songImage;
            TextView artist;
            TextView name;
            TextView songId;
        }

        public void add(Song song) {
            listData.add(song);
            this.notifyDataSetChanged();

        }
        public void removesAllSongsAndAddNewOnes(ArrayList<Song> songList)
        {
            listData.clear();
            listData.addAll(songList);
            this.notifyDataSetChanged();
        }
        public ArrayList<Song> getData()
        {
            return listData;
        }
    }




}
