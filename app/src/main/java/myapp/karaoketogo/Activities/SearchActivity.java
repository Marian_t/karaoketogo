package myapp.karaoketogo.Activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class SearchActivity extends BaseMenuActivity {
    CustomListAdapter songsAdapter;
    ArrayList<Song> data;
    ListView listView;

    YouTubeActivity youtubeActivity;

    private MyProgressBar progressBar;

    boolean isTextSubmited;



    public SearchActivity() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        progressBar=new MyProgressBar(this);
        progressBar.showProgressDialog();

        youtubeActivity=new YouTubeActivity();

        //after the data arrived from server,we populate the listView
        data= new ArrayList<Song>();
        isTextSubmited=false;


        listView= (ListView)findViewById(R.id.Activity_search_liatOfSuggestions);
        songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), data);
        listView.setAdapter(songsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Song songInPosition = new Song((Song) songsAdapter.getItem(position));
                ArrayList<Song> alToSend=new ArrayList<Song>();

                //make a different arrayList from songs adapter list
                for(Song s:songsAdapter.getData())
                {
                    alToSend.add(new Song(s));
                }


                //put null in bitmap,because its problem to send it to another activity,I will download the songs there
                songInPosition.setSongImage(null);
                putNullInBitMaps(alToSend);

                Intent intent=new Intent(MyAppContext.getAppContext(),YouTubeActivity.class);



                Bundle bundle = new Bundle();
                bundle.putSerializable("songInPosition", songInPosition);
                bundle.putSerializable("searchSuggestions",alToSend);
                intent.putExtras(bundle);

                startActivity(intent);


            }
        });

        Model.instance().getAllSongs(new Model.GetAllSongsListener() {
            @Override
            public void onComplete(Song song) {
                songsAdapter.add(song);
            }

            @Override
            public Context getAppContext() {
                return MyAppContext.getAppContext();
            }

            @Override
            public void showProgressBar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressBar() {
                progressBar.hideProgressDialog();
            }

        });


        final SearchView searchView = (SearchView) findViewById(R.id.Activity_search_search);
        searchView.setQueryHint(getString(R.string.search_hint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
//                searchInSongs(newText);
//                return false;
                if(!newText.isEmpty())
                {
                    isTextSubmited=true;
                    searchInSongs(newText);
                }
                else
                {
                    isTextSubmited=false;
                    songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), data);
                    listView.setAdapter(songsAdapter);
                }

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty())
                {
                    searchView.setQuery("", false);
                    searchInSongs(query);
                    isTextSubmited=true;
                    searchView.setIconified(true);//triger on close listener
                }
                else
                {
                    isTextSubmited=false;
                    songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), data);
                    listView.setAdapter(songsAdapter);
                }

                return true;
//                searchInSongs(query);
//                searchView.setQuery("", false);
//                return false;
            }

        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //songsAdapter.removesAllSongsAndAddNewOnes(data);
                if(!isTextSubmited)
                {
                    songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), data);
                    listView.setAdapter(songsAdapter);
                }


                return true;
            }
        });



    }





    public void searchInSongs(String text)
    {
        ArrayList<Song> searchData=new ArrayList<>();
        boolean found=false;
        for(Song s:data)
        {
            //check if s.getName contains text ignoring lower and upper cases
            if(Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE).matcher(s.getName()).find())
            {
                found=true;
            }
            if(Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE).matcher(s.getArtist()).find())
            {
                found=true;
            }
            String artist_name=s.getArtist()+"-"+s.getName();
            if(Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE).matcher(artist_name).find())
            {
                found=true;
            }
            else
            {
                found=false;
            }

            if(found)
            {
                searchData.add(s);
            }
            found=false;
        }
        songsAdapter=new CustomListAdapter(MyAppContext.getAppContext(), searchData);
        listView.setAdapter(songsAdapter);
    }

    public void putNullInBitMaps(ArrayList<Song> al)
    {
        for(Object s:al)
        {
            Song song=(Song)s;
            song.setSongImage(null);
        }
    }




    public class CustomListAdapter extends BaseAdapter {
        private ArrayList listData;
        private LayoutInflater layoutInflater;

        public CustomListAdapter(Context context, ArrayList listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.song_list_row, null);
                holder = new ViewHolder();
                holder.songImage= (ImageView) convertView.findViewById(R.id.song_list_row_image_IV);
                holder.artist= (TextView) convertView.findViewById(R.id.song_list_row_artist_TV);
                holder.name = (TextView) convertView.findViewById(R.id.song_list_row_name_TV);
                holder.songId=(TextView)convertView.findViewById(R.id.song_list_row_id_TV);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if(listData==null)
            {
                return convertView;
            }
            Song songInPosition = (Song) listData.get(position);
            holder.name.setText(songInPosition.getName());
            holder.artist.setText(songInPosition.getArtist());
            holder.songImage.setImageBitmap(songInPosition.getSongImage());
            holder.songId.setText(songInPosition.getSongId());

            return convertView;
        }

        class ViewHolder {
            ImageView songImage;
            TextView artist;
            TextView name;
            TextView songId;
        }

        public void add(Song song) {
            listData.add(song);
            this.notifyDataSetChanged();

        }

        public ArrayList<Song> getData()
        {
            return listData;
        }


    }


}
