package myapp.karaoketogo.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import myapp.karaoketogo.Constants;
import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.CompletedSrt;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class BuyKaraokeActivity extends BaseMenuActivity {
    MySongsAdapter songsAdapter;
    ArrayList<CompletedSrt> data;
    ListView listView;

    TextView pointTV;
    TextView songName;
    TextView songArtist;
    Button mySongsBtn;
    Button allSongsBtn;

    int pointsOfCurrentUser;

    MyProgressBar progressBar;
    Song song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_karaoke);

        progressBar=new MyProgressBar(this);

        pointTV=(TextView)findViewById(R.id.activity_buy_points_TV);
        songName=(TextView)findViewById(R.id.activity_buy_songName_tv);
        songArtist=(TextView)findViewById(R.id.activity_buy_songArtist_tv);
        mySongsBtn=(Button)findViewById(R.id.activity_buy_goToMySong_btn);
        allSongsBtn=(Button)findViewById(R.id.activity_buy_goToAllSongs_btn);

        mySongsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(MyAppContext.getAppContext(),Mysongs.class);
                finish();
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);

            }
        });
        allSongsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3=new Intent(MyAppContext.getAppContext(),SearchActivity.class);
                finish();
                intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent3);

            }
        });


        Intent i = getIntent();
        song = (Song)i.getSerializableExtra("song");
        songName.setText(song.getName());
        songArtist.setText(song.getArtist());

        data= new ArrayList<CompletedSrt>();


        listView= (ListView)findViewById(R.id.activity_buy_timingOfSong_lv);
        songsAdapter=new MySongsAdapter(MyAppContext.getAppContext(), data);
        listView.setAdapter(songsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                if(pointsOfCurrentUser- Constants.pointsForSong <0)
                {
                    Toast.makeText(MyAppContext.getAppContext(), "You have not enogth points to buy a song!", Toast.LENGTH_LONG).show();
                    return;
                }

                final CompletedSrt currentSrt = new CompletedSrt((CompletedSrt) songsAdapter.getItem(position));

                final ArrayList<CompletedSrt> al=new ArrayList<CompletedSrt>();
                al.add(currentSrt);
//                Song songInPosition = new Song((Song) songsAdapter.getItem(position));
//
//                //put null in bitmap,because its problem to send it to another activity,I will download the songs there
//                songInPosition.setSongImage(null);
//
//                Intent intent=new Intent(MyAppContext.getAppContext(),ShowKaraokeActivity.class);
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("songInPosition", songInPosition);
//                intent.putExtras(bundle);
//
//                startActivity(intent);

                final AlertDialog.Builder builder = new AlertDialog.Builder(BuyKaraokeActivity.this);
                builder.setTitle("Title");
                builder.setItems(new CharSequence[]
                                {"Yes,And than listen the song", "Yes,Go to MySongs", "Yes,Go to All Songs", "No"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:
                                        song.setCompletedSrts(al);
                                        buySongInModel(currentSrt);
                                        Intent intent1=new Intent(MyAppContext.getAppContext(),ShowKaraokeActivity.class);
                                        Bundle bundle = new Bundle();
                                        //intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        bundle.putSerializable("song", song);
                                        intent1.putExtras(bundle);

                                        startActivity(intent1);


                                        break;
                                    case 1:
                                        buySongInModel(currentSrt);
                                        Intent intent2=new Intent(MyAppContext.getAppContext(),Mysongs.class);
                                        //intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent2);
                                        finish();

                                        break;
                                    case 2:
                                        buySongInModel(currentSrt);
                                        Intent intent3=new Intent(MyAppContext.getAppContext(),SearchActivity.class);
                                        //intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent3);
                                        finish();
                                        break;
                                    case 3:
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });
                builder.setTitle("Are you sure you want to buy this song?");
                builder.create().show();

            }
        });

        Model.instance().getAllSrtOfOneSong(song, new Model.GetAllSrtOfOneSongListener() {
            @Override
            public void setPoints(int points) {
                pointTV.setText(String.valueOf(points));
                pointsOfCurrentUser=points;
            }

            @Override
            public void addSrt(CompletedSrt cs) {
                if(cs==null)
                {

                    return;
                }
                songsAdapter.add(cs);
            }

            @Override
            public void showProgressbar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressbar() {
                progressBar.hideProgressDialog();
            }
        });
    }

    public void buySongInModel(CompletedSrt currentSrt)
    {
        Model.instance().buySong(song,currentSrt,pointsOfCurrentUser);
    }


    public class MySongsAdapter extends BaseAdapter {
        private ArrayList<CompletedSrt> listData;
        private LayoutInflater layoutInflater;

        public MySongsAdapter(Context context, ArrayList listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.timing_of_song_raw, null);
                holder = new ViewHolder();
                holder.ownerLName=(TextView)convertView.findViewById(R.id.timingOfSong_row_ownerLName_TV);
                holder.timingId=(TextView)convertView.findViewById(R.id.timingOfSong_row_id_TV);
                holder.ownerFName=(TextView)convertView.findViewById(R.id.timingOfSong_row_ownerFName_TV);
                holder.numbering=(TextView)convertView.findViewById(R.id.timingOfSong_row_numbering_TV);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if(listData==null)
            {
                return convertView;
            }
            CompletedSrt srtInPlace = (CompletedSrt) listData.get(position);
            holder.ownerFName.setText(srtInPlace.getOwnerFirstName());
            holder.ownerLName.setText(srtInPlace.getOwnerLastName());
            holder.timingId.setText(srtInPlace.getSrtId());
            holder.numbering.setText(position+1+".  ");
            return convertView;
        }

        class ViewHolder {
            TextView ownerLName;
            TextView timingId;
            TextView ownerFName;
            TextView numbering;
        }

        public void add(CompletedSrt srt) {
            listData.add(srt);
            this.notifyDataSetChanged();

        }

        public ArrayList<CompletedSrt> getData()
        {
            return listData;
        }


    }

}
