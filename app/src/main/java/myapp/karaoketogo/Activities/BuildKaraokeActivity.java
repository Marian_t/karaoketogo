package myapp.karaoketogo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.LineOfSong;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.Model.TextOfSong;
import myapp.karaoketogo.Model.TimingOfSong;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class BuildKaraokeActivity extends BaseMenuActivity implements MediaPlayer.OnTimedTextListener{
    Button finishButton;
    ImageButton playButton;
    ImageButton pauseButton;
    ImageButton stopButton;
    Button synchButton;
    TextView currentTime;
    TextView totalTime;

    Runnable runnable;
    SeekBar seekBar;
    private static Handler handler = new Handler();

    TextView lineStart;
    TextView lineCenter;
    TextView lineEnd;
    MyProgressBar progressBar;

    TimingOfSong TOSongs;
    int lineNumber=0;//current line number shown in textView
    boolean isFirst;//if the first word in line is marked red


    MediaPlayer player;

    int playerState;//1-nothing,2-playing,3-was stopped,4-was paused

    Song song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build_karaoke);
        progressBar=new MyProgressBar(this);
        progressBar.showProgressDialog();
        player = new  MediaPlayer();

        seekBar = (SeekBar) findViewById(R.id.SeekBar);

        finishButton=(Button)findViewById(R.id.activity_buildKaraoke_finish_button);
        playButton=(ImageButton)findViewById(R.id.activity_build_karaoke_play_btn);
        pauseButton=(ImageButton)findViewById(R.id.activity_build_karaoke_pause_btn);
        stopButton=(ImageButton)findViewById(R.id.activity_build_karaoke_stop_btn);
        synchButton=(Button)findViewById(R.id.activity_build_karaoke_synching_button);

        synchButton.setEnabled(false);
        //finishButton.setVisibility(View.GONE);

        currentTime=(TextView)findViewById(R.id.activity_build_karaoke_currentTime_TV);
        totalTime=(TextView)findViewById(R.id.activity_build_karaoke_totalTime_TV);


        lineStart=(TextView)findViewById(R.id.activity_build_karaoke_tv_1);
        lineCenter=(TextView)findViewById(R.id.activity_build_karaoke_tv_2);
        lineEnd=(TextView)findViewById(R.id.activity_build_karaoke_tv_3);

        playerState=1;
        TOSongs=new TimingOfSong();

        Intent i = getIntent();
        song = (Song)i.getSerializableExtra("song");
        Model.instance().getSongsTxtAndSong(song,new Model.DownloadTxtAndFullSongListener()
        {
            @Override
            public void playSongInMediaPlayerAndHandleTXT(Song song) {

                String songFilePath = song.getFullSongFileNameLocal();
                String TxtFilePath=song.getLyricsTXTFileNameLocal();

                //get line by line of the lyrics to the TimingOfSongs object
                File file = new File(TxtFilePath);

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));

                    String line = null;
                    while ((line = br.readLine()) != null) {
                        if(line.contains("\uFFFD"))
                        {
                            line= line.replaceAll("\uFFFD", "-");
                        }


                        if (line.trim().equals("")) {
                            continue;
                        } else {
                            TOSongs.addToArrayOfSongs(new LineOfSong(line.trim(),-1,-1));
                        }
                    }
                    br.close();

                    String firstLine=TOSongs.getElementFromArray(0).getLine();
                    ArrayList<String> al=devideLineTo3(firstLine);
                    isFirst=true;//right now we on the first word
                    lineStart.setText(al.get(0));
                    lineStart.setTextColor(getResources().getColor(R.color.red));
                    lineCenter.setText(al.get(1));
                    lineEnd.setText(al.get(2));

                    //play the file from the path of the song
                    player.setDataSource(songFilePath);

                    //player.prepare();
                    //player.start();
                    progressBar.hideProgressDialog();
                }
                catch (IOException e) {
                    //You'll need to add proper error handling here
                }

            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if(input)
                {
                    if(player!=null && player.isPlaying())
                    {

                        if(player.getCurrentPosition()<progress)//move thumb to right
                        {
                            //player.seekTo(progress);
                            int currentTimeInSeekBar=findEmptyPlaceInSrt();
                            if(currentTimeInSeekBar==-1)//there is no empty space..the user timed till the end of the song
                            {
                                player.seekTo(progress);
                            }
                            else
                            {
                                player.seekTo(currentTimeInSeekBar);
                            }
                        }
                        else if(player.getCurrentPosition()>progress)//move thumb to left
                        {
                            int x=findPlaceNearProgress(progress);

                            player.seekTo(x);


                        }
                        else
                        {
                            player.seekTo(progress);
                        }

                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(player!= null && player.isPlaying())
                {
                    player.pause();
                    playerState=4;
                    synchButton.setEnabled(false);
                }
                progressBar.showProgressDialog();

                //--------handle algorithm and write to srt file-------------


                TextOfSong textOfSong=new TextOfSong(song);
                textOfSong.insertSubtitleToSrtFile(TOSongs);
                //----------------------------------------------------

                progressBar.hideProgressDialog();
                Intent intent=new Intent(MyAppContext.getAppContext(),ShowTimingActivity.class);

                intent.putExtra("song",song);
                startActivity(intent);


            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //songStartTime=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss,SSS").format(new Date());
                //TOSongs.setStartSong(songStartTime);


                synchButton.setEnabled(true);

                try {
                    if((player==null)&&(playerState==3))//if player was stopped
                    {
                        player=new MediaPlayer();
                        player.setDataSource(song.getFullSongFileNameLocal());
                        player.prepare();
                        player.start();

                    }
                    else if(playerState==4)
                    {
                        player.start();
                    }
                    else if(playerState==1)
                    {
                        player.prepare();
                        player.start();
                        totalTime.setText(getTimeString(player.getDuration()));



                    }
                    seekBar.setMax(player.getDuration());
                    playCycle();

                } catch (IOException e) {
                    e.printStackTrace();
                }


                playerState=2;




            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(player!=null && player.isPlaying())
                {
                    player.release();
                    handler.removeCallbacks(runnable);
                    player=null;
                    playerState=3;

                }
                for(LineOfSong s:TOSongs.getArrayOfSong())
                {
                    s.setStartTimeOfLine(-1);
                    s.setEndTimeOfLine(-1);
                    lineNumber=0;
                    isFirst=true;
                }
                String firstLine=TOSongs.getElementFromArray(0).getLine();
                ArrayList<String> al=devideLineTo3(firstLine);
                isFirst=true;//right now we on the first word
                lineStart.setText(al.get(0));
                lineStart.setTextColor(getResources().getColor(R.color.red));
                lineCenter.setText(al.get(1));
                lineEnd.setText(al.get(2));
                lineEnd.setTextColor(getResources().getColor(R.color.black));
                synchButton.setEnabled(false);

            }
        });
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(player!= null && player.isPlaying())
                {
                    player.pause();
                    playerState=4;
                    synchButton.setEnabled(false);
                }

//                playCycle();

            }
        });

        synchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int timeNow=player.getCurrentPosition();

                //----------one word problem------
                String[] arr=TOSongs.getArrayOfSong().get(lineNumber).getLine().split(" ");
                if(arr.length==1)
                {

                    if(isFirst)
                    {
                        TOSongs.getArrayOfSong().get(lineNumber).setEndTimeOfLine(timeNow+1000);
                        TOSongs.getArrayOfSong().get(lineNumber).setStartTimeOfLine(timeNow-300);
                        lineNumber++;
                        if(lineNumber<TOSongs.getArrayOfSong().size())
                        {
                            String currentLine=TOSongs.getArrayOfSong().get(lineNumber).getLine();
                            ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                            lineStart.setText(alLineDevided.get(0));
                            lineStart.setTextColor(getResources().getColor(R.color.red));
                            lineEnd.setTextColor(getResources().getColor(R.color.black));

                            lineCenter.setText(alLineDevided.get(1));

                            lineEnd.setText(alLineDevided.get(2));
                            isFirst=true;
                        }
                        else
                        {
                            synchButton.setText("You Have Finished Timing the Song!");
                            synchButton.setEnabled(false);
                            // finishButton.setVisibility(View.VISIBLE);
                            lineStart.setText("");
                            lineCenter.setText("");
                            lineEnd.setText("");
                        }


                    }
                    return;
                }

                //handle first line
//                if(lineNumber==0)
//                {
//                    if(isFirst)
//                    {
//                        TOSongs.getArrayOfSong().get(0).setStartTimeOfLine(timeNow);
//                        lineEnd.setTextColor(getResources().getColor(R.color.red));
//                        lineStart.setTextColor(getResources().getColor(R.color.black));
//                        isFirst=false;
//                    }
//                    else
//                    {
//                        TOSongs.getArrayOfSong().get(0).setEndTimeOfLine(timeNow);
//                        lineNumber++;
//                        String currentLine=TOSongs.getArrayOfSong().get(lineNumber).getLine();
//                        ArrayList<String> alLineDevided=devideLineTo3(currentLine);
//                        lineStart.setText(alLineDevided.get(0));
//                        lineStart.setTextColor(getResources().getColor(R.color.red));
//                        lineEnd.setTextColor(getResources().getColor(R.color.black));
//
//                        lineCenter.setText(alLineDevided.get(1));
//
//                        lineEnd.setText(alLineDevided.get(2));
//                        isFirst=true;
//                    }
//                    return;
//
//                }

                //handle other lines
                if(isFirst)//the first word is marked,now lets mark the second word
                {
                    if(lineNumber<TOSongs.getArrayOfSong().size())
                    {
                        TOSongs.getArrayOfSong().get(lineNumber).setStartTimeOfLine(timeNow-1000);
                        lineEnd.setTextColor(getResources().getColor(R.color.red));
                        lineStart.setTextColor(getResources().getColor(R.color.black));
                        isFirst=false;
                    }
                    else
                    {
                        synchButton.setText("You Have Finished Timing the Song!");
                        synchButton.setEnabled(false);

                        lineStart.setText("");
                        lineCenter.setText("");
                        lineEnd.setText("");
                    }

                }
                else
                {
                    TOSongs.getArrayOfSong().get(lineNumber).setEndTimeOfLine(timeNow+1000);
                    lineNumber++;
                    if(lineNumber<TOSongs.getArrayOfSong().size())
                    {
                        String currentLine=TOSongs.getArrayOfSong().get(lineNumber).getLine();
                        ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                        lineStart.setText(alLineDevided.get(0));
                        lineStart.setTextColor(getResources().getColor(R.color.red));
                        lineEnd.setTextColor(getResources().getColor(R.color.black));

                        lineCenter.setText(alLineDevided.get(1));

                        lineEnd.setText(alLineDevided.get(2));
                        isFirst=true;

                    }
                    else
                    {
                        synchButton.setText("You Have Finished Timing the Song!");
                        synchButton.setEnabled(false);
                        //finishButton.setVisibility(View.VISIBLE);
                        lineStart.setText("");
                        lineCenter.setText("");
                        lineEnd.setText("");
                    }
                }

            }
        });
    }

    public void  playCycle(){

        if(player!=null && player.isPlaying())
        {
            currentTime.setText(getTimeString(player.getCurrentPosition()));
            seekBar.setProgress(player.getCurrentPosition());
            runnable=new Runnable() {
                @Override
                public void run() {
                    playCycle();

                }
            };
            handler.postDelayed(runnable,1000);
        }
//        seekBar.setProgress(player.getCurrentPosition());
//
//        if(player!=null && player.isPlaying())
//        {
//            runnable=new Runnable() {
//                @Override
//                public void run() {
//                    playCycle();
//                }
//            };
//            handler.postDelayed(runnable,1000);
//        }
    }

    private String getTimeString(int millis) {
        StringBuffer buf = new StringBuffer();

        int minutes = (millis % (1000*60*60) ) / (1000*60);
        int seconds = ( ( millis % (1000*60*60) ) % (1000*60) ) / 1000;

        buf
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        if(player!=null && player.isPlaying())
        {
            player.pause();
            playerState=4;
            synchButton.setEnabled(false);
        }

    }
    @Override
    public void onResume()
    {
        super.onResume();
    }


    @Override
    public void onTimedText(MediaPlayer mp, TimedText text) {

    }

    public ArrayList<String> devideLineTo3(String line)
    {
        ArrayList<String> al=new ArrayList<>();
        if(line.length()!=0)
        {

            String[] arr = line.split(" ");
            if(arr.length==1)
            {
                al.add(line);
                al.add("");
                al.add("");
                return al;
            }
            if(arr.length==2)
            {
                al.add(arr[0]);
                al.add(" ");
                al.add(arr[1]);
                return al;
            }


            String firstWord=line.substring(0,line.indexOf(" ")+1);//taking first word with space
            al.add(firstWord);
            String center=line.substring(line.indexOf(" ")+1,line.lastIndexOf(" "));
            al.add(center);
            String lastWord=line.substring(line.lastIndexOf(" "));//taking last word with space
            al.add(lastWord);
        }

        return al;
    }

    /**
     * find the first place where there is -1 in the array,and change the subtitles according the current line before -1
     * @return the line before end time +1 millis
     */
    public int findEmptyPlaceInSrt()
    {
        for(int i=0;i<TOSongs.getArrayOfSong().size();i++)
        {
            LineOfSong l=TOSongs.getArrayOfSong().get(i);
            if(l.getStartTimeOfLine()==-1 || l.getEndTimeOfLine()==-1)
            {
                if(i==0)
                {
                    return 0;
                }
                else
                {
                    String currentLine=TOSongs.getArrayOfSong().get(i).getLine();
                    ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                    lineStart.setText(alLineDevided.get(0));
                    lineStart.setTextColor(getResources().getColor(R.color.red));
                    lineEnd.setTextColor(getResources().getColor(R.color.black));

                    lineCenter.setText(alLineDevided.get(1));

                    lineEnd.setText(alLineDevided.get(2));
                    isFirst=true;
                    lineNumber=i;
                    LineOfSong preLine=TOSongs.getArrayOfSong().get(i-1);

                    return preLine.getEndTimeOfLine()+1;

                }
            }
        }
        return -1;//if all the srt is already timed

    }
    public int findPlaceNearProgress(int progress)
    {
        for(int i=0;i<TOSongs.getArrayOfSong().size();i++) {
            LineOfSong l = TOSongs.getArrayOfSong().get(i);

            if((l.getStartTimeOfLine()==-1)&&(l.getEndTimeOfLine()==-1))///we arrive to the point the user didn't synched
            {
                return progress;
            }

            if(progress>l.getStartTimeOfLine() && l.getEndTimeOfLine()==-1)//arrived to the point he didn't synched in the middle of the line
            {
                String currentLine=TOSongs.getArrayOfSong().get(i).getLine();
                ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                lineStart.setText(alLineDevided.get(0));
                lineStart.setTextColor(getResources().getColor(R.color.red));
                lineEnd.setTextColor(getResources().getColor(R.color.black));

                lineCenter.setText(alLineDevided.get(1));

                lineEnd.setText(alLineDevided.get(2));
                isFirst=true;
                lineNumber=i;
                if(i==0)
                {
                    return progress;
                }
                LineOfSong preLine=TOSongs.getArrayOfSong().get(i-1);

                return preLine.getEndTimeOfLine()+1;
            }


            if(progress<l.getStartTimeOfLine())
            {
                String currentLine=TOSongs.getArrayOfSong().get(i).getLine();
                ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                lineStart.setText(alLineDevided.get(0));
                lineStart.setTextColor(getResources().getColor(R.color.red));
                lineEnd.setTextColor(getResources().getColor(R.color.black));

                lineCenter.setText(alLineDevided.get(1));

                lineEnd.setText(alLineDevided.get(2));
                isFirst=true;
                lineNumber=i;
                if(i==0)//if we are between the start of the song and the first line
                {
                    return progress;
                }
                LineOfSong preLine=TOSongs.getArrayOfSong().get(i-1);

                return preLine.getEndTimeOfLine()+1;
            }
            else if(progress>l.getStartTimeOfLine() && progress<l.getEndTimeOfLine())
            {
                String currentLine=TOSongs.getArrayOfSong().get(i).getLine();
                ArrayList<String> alLineDevided=devideLineTo3(currentLine);
                lineStart.setText(alLineDevided.get(0));
                lineStart.setTextColor(getResources().getColor(R.color.red));
                lineEnd.setTextColor(getResources().getColor(R.color.black));

                lineCenter.setText(alLineDevided.get(1));

                lineEnd.setText(alLineDevided.get(2));
                isFirst=true;
                lineNumber=i;
                if(i==0)
                {
                    return progress;
                }
                LineOfSong preLine=TOSongs.getArrayOfSong().get(i-1);

                return preLine.getEndTimeOfLine()+1;
            }

        }

        return progress;
    }


}
