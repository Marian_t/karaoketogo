package myapp.karaoketogo.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import myapp.karaoketogo.Dialogs.MyProgressBar;
import myapp.karaoketogo.Model.Model;
import myapp.karaoketogo.Model.Song;
import myapp.karaoketogo.Model.User;
import myapp.karaoketogo.MyAppContext;
import myapp.karaoketogo.R;

public class Mysongs extends BaseMenuActivity {

    MySongsAdapter songsAdapter;
    ArrayList<Song> data;
    ListView listView;

    TextView firstNameTV;
    TextView lastNameTV;
    TextView pointsTV;

    private MyProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mysongs);

        progressBar=new MyProgressBar(this);


        firstNameTV=(TextView)findViewById(R.id.mysongsFName);
        lastNameTV=(TextView)findViewById(R.id.mysongsLName);
        pointsTV=((TextView)findViewById(R.id.mysongsPoints));

        data= new ArrayList<Song>();


        listView= (ListView)findViewById(R.id.mysongsList);
        songsAdapter=new MySongsAdapter(MyAppContext.getAppContext(), data);
        listView.setAdapter(songsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Song songInPosition = new Song((Song) songsAdapter.getItem(position));

                //put null in bitmap,because its problem to send it to another activity,I will download the songs there
                songInPosition.setSongImage(null);

                Intent intent=new Intent(MyAppContext.getAppContext(),ShowKaraokeActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("song", songInPosition);
                intent.putExtras(bundle);

                startActivity(intent);

            }
        });

        Model.instance().getUserDetailsAndSongs(new Model.GetUserDetailsAndSongsListener() {
            @Override
            public void addSongToList(Song s) {
                songsAdapter.add(s);
            }

            @Override
            public void addUserDetails(User user) {
                firstNameTV.setText(user.getFirstName());
                lastNameTV.setText(user.getLastName());
                pointsTV.setText(String.valueOf(user.getPoints()));
            }

            @Override
            public void showProgressbar() {
                progressBar.showProgressDialog();
            }

            @Override
            public void hideProgressbar() {
                progressBar.hideProgressDialog();
            }
        });


    }
    public class MySongsAdapter extends BaseAdapter {
        private ArrayList listData;
        private LayoutInflater layoutInflater;

        public MySongsAdapter(Context context, ArrayList listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            return listData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.mysongs_row, null);
                holder = new ViewHolder();
                holder.songImage= (ImageView) convertView.findViewById(R.id.mySongs_row_image_IV);
                holder.artist= (TextView) convertView.findViewById(R.id.mySongs_row_artist_TV);
                holder.name = (TextView) convertView.findViewById(R.id.mySongs_row_name_TV);
                holder.songId=(TextView)convertView.findViewById(R.id.mySongs_row_id_TV);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if(listData==null)
            {
                return convertView;
            }
            Song songInPosition = (Song) listData.get(position);
            holder.name.setText(songInPosition.getName());
            holder.artist.setText(songInPosition.getArtist());
            holder.songImage.setImageBitmap(songInPosition.getSongImage());
            holder.songId.setText(songInPosition.getSongId());

            return convertView;
        }

        class ViewHolder {
            ImageView songImage;
            TextView artist;
            TextView name;
            TextView songId;
        }

        public void add(Song song) {
            listData.add(song);
            this.notifyDataSetChanged();

        }

        public ArrayList<Song> getData()
        {
            return listData;
        }


    }

}